import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ClientesModel, LectorModel, BitacoraLector } from '../models/lector/lector.model';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class LectoresService {

  constructor(private http: HttpClient , private auth: AuthService) { }

  getClientes(){
    let array = [];
    return this.http.get<[]>(
      `${this.auth.url}lectores/index` ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          array=resp;
          return array;
        }
      )
    );
  }

  getAllLectores(){
    let array = [];
    return this.http.get<[]>(
      `${this.auth.url}lectoresCron/ejecutaRefresh`,
      { headers: { 'Authorization': `Bearer ${this.auth.userToken}` } },
    ).pipe(
      map(
        resp => {
          array=resp;
          return array;
        }
      )
    );
  }

  getLectoresByCliente(data){
    let authData = {
      ...data
    };
    let array = [];
    let resp = '';
    return this.http.post<[]>(
      `${this.auth.url}lectores/getbitacoraBySerie` , authData,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          resp.forEach(
            (elem: any) => {
              elem = {
                ...elem,
                ejecutado : 0
              };
              array.push(elem);
            }
          );
          return array;
        }
      )
    );
  }

  savebitacoraByCliente(data){
    let authData = {
      ...data
    };
    let array = [];
    return this.http.post<[]>(
      `${this.auth.url}lectores/savebitacoraBySerie` , authData,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  updateBitacora(data){
    let authData = {
      ...data
    };
    return this.http.post<[]>(
      `${this.auth.url}lectores/updateBitacora`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }
}
