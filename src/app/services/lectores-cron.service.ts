import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LectoresCronService {

  constructor(private http: HttpClient, private auth: AuthService) { }

  getAllLecCrons() {
    let array = [];
    return this.http.get<[]>(
      `${this.auth.url}lectoresCron/index`,
      { headers: { 'Authorization': `Bearer ${this.auth.userToken}` } },
    ).pipe(
      map(
        resp => {
          resp.forEach(
            (elem: any) => {
              /*              if (elem['password'] === null) {
                              elem['password']='';
                            }*/
              array.push(elem);
            }
          );
          return array;
        }
      )
    );
  }

  update(data) {
    let authData = {
      ...data
    };
    return this.http.post<[]>(
      `${this.auth.url}lectoresCron/update`, authData,
      { headers: { 'Authorization': `Bearer ${this.auth.userToken}` } },
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );

  }

  getAllHorasCron(data){
    let authData = {
      ...data
    };
    let array = [];
    return this.http.post<[]>(
      `${this.auth.url}lectoresCron/getAllHorasCron`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          console.log(resp);
          resp.forEach(
            (elem: any) => {
              array.push(elem);
            }
          );
          return array;
        }
      )
    );
  }

  save(data) {
    let authData = {
      ...data
    };
    return this.http.post<[]>(
      `${this.auth.url}lectoresCron/save`, authData,
      { headers: { 'Authorization': `Bearer ${this.auth.userToken}` } },
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  updateCronHoras( data ){
    let authData = {
      ...data
    };
    return this.http.post<[]>(
      `${this.auth.url}lectoresCron/updateCronHoras`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

}
