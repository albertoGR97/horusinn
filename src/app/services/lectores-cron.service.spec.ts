import { TestBed } from '@angular/core/testing';

import { LectoresCronService } from './lectores-cron.service';

describe('LectoresCronService', () => {
  let service: LectoresCronService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LectoresCronService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
