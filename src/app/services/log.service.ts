import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { LogModel } from '../models/log/log.model';
import { map } from 'rxjs/operators';
import { UsuarioModel } from '../models/usuario/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private http: HttpClient, private auth: AuthService) { }

  crearLog(log: LogModel) {
    let logData = {
      ...log
    };

    return this.http.post(
      `${this.auth.url}log/nuevo`, logData,
      { headers: { 'Authorization': `Bearer ${this.auth.userToken}` } },
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  actualizarCampo(log: LogModel){
    let logData = {
      ...log
    };

    return this.http.post(
      `${this.auth.url}log/actualizar`, logData,
      { headers: { 'Authorization': `Bearer ${this.auth.userToken}` } },
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  obtenerUsuario(){
    this.auth.user().subscribe(resp=>{
      sessionStorage.setItem('id_user', resp['id']);
    }, error=>{
      sessionStorage.setItem('id_user', '0');
    });
  }
}
