import { Injectable } from '@angular/core';
// import {ServidoresModel} from '../models/servidor/servidores.model';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';
import {AmbienteModel} from '../models/ambiente/ambiente.model';

@Injectable({
  providedIn: 'root'
})
export class AmbientesService {

  constructor(private http: HttpClient , private auth: AuthService) { }


  //herby conexion de api con frontend y backend
  //ingresar al proyecto de horus desde frontend
  getclientes_direccion(){
   
    let ClienteDir =[];
    return this.http.get<[]>(
      `${this.auth.url}ambientes/getAllClienteDireccion`,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          ClienteDir = resp;
          return ClienteDir;
        }
      )
    )
  }

  getTablesName(ambiente: AmbienteModel ){
    let authData = {
      ...ambiente
    };
    let tablas =[];
    return this.http.post<[]>(
      `${this.auth.url}ambientes/getTablesName`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(

        resp => {
          resp.forEach(
            (elem: any) => {
              elem = {
                ...elem,
                ejecutado : 0,
              };
              tablas.push(elem);
            }
          );
          return tablas;
        }
      )
    );
  }
  getFunciones(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    let array = [];
    return this.http.post<[]>(
      `${this.auth.url}ambientes/getFunciones`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          resp.forEach(
            (elem: any) => {
              elem = {
                ...elem,
                ejecutado : 0
              };
              array.push(elem);
            }
          );
          return array;
        }
      )
    );
  }
  getEvents(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    let array = [];
    return this.http.post<[]>(
      `${this.auth.url}ambientes/getEvents`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          resp.forEach(
            (elem: any) => {
              elem = {
                ...elem,
                ejecutado : 0
              };
              array.push(elem);
            }
          );
          return array;
        }
      )
    );

  }
  getSP(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    let array=[];
    return this.http.post<[]>(
      `${this.auth.url}ambientes/getSP`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          resp.forEach(
            (elem: any) => {
              elem = {
                ...elem,
                ejecutado : 0
              };
              array.push(elem);
            }
          );
          return array;
        }
      )
    );

  }
  getTriggers(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    let array = [];
    return this.http.post<[]>(
      `${this.auth.url}ambientes/getTriggers`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          resp.forEach(
            (elem: any) => {
              elem = {
                ...elem,
                ejecutado : 0
              };
              array.push(elem);
            }
          );
          return array;
        }
      )
    );

  }

  getViews(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    let array = [];
    return this.http.post<[]>(
      `${this.auth.url}ambientes/getViews`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          resp.forEach(
            (elem: any) => {
              elem = {
                ...elem,
                ejecutado : 0,
              };
              array.push(elem);
            }
          );
          return array;
        }
      )
    );

  }

  creaBase(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/creaBase`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

    creaTablas(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/creaTablas`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  creaFunciones(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/creaFunciones`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  creaEvents(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/creaEvents`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  creaSP(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/creaSP`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  creaTriggers(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/creaTriggers`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  creaViews(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/creaViews`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }
  // getTareasAdicionales(ambiente: LiberAmbienteModel ){
  //   const authData = {
  //     ...ambiente
  //   };
  //   let array = [];
  //   return this.http.post(
  //     `${this.auth.url}tareas/getByProduct`, authData ,
  //     {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
  //   ).pipe(
  //     map(
  //       resp => {
  //         resp['tareas'].forEach(
  //           (elem: any) => {
  //             elem = {
  //               ...elem,
  //               ejecutado : 0
  //             };
  //             array.push(elem);
  //           }
  //         );
  //         return array;
  //       }
  //     )
  //   );

  // }
  getTareasAdicionales(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    let array = [];
    return this.http.post<[]>(
      `${this.auth.url}ambientes/getTareasAdicionales`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          console.log(resp);
          resp.forEach(
            (elem: any) => {
              elem = {
                ...elem,
                ejecutado : 0
              };
              array.push(elem);
            }
          );
          return array;
        }
      )
    );

  }

  getUltimosPasos(ambiente: AmbienteModel){
    const authData = {
      ...ambiente
    };
    let array = [];
    return this.http.post<[]>(
      `${this.auth.url}ambientes/getUltimosPasos`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          console.log(resp);
          resp.forEach(
            (elem: any) => {
              elem = {
                ...elem,
                ejecutado : 0
              };
              array.push(elem);
            }
          );
          let correo = {Name:'enviar_correo', ejecutado: 0};
          array.push(correo);
          return array;
        }
      )
    );
  }

  creaInformacionDefault(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/creaInformacionDefault`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  destruyeAmbiente(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/destruyeAmbiente`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  getLastCliente(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/getLastCliente`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }
  getAllClientes(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/getAllClientes`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }



  getServerbyProduct(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    let array = [];
    return this.http.post<[]>(
      `${this.auth.url}server/getServerbyProduct`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          resp.forEach(
            (elem: any) => {
              elem = {
                ...elem,
                name : elem.nombre,
                code : elem.nombre,
              };
              array.push(elem);
            }
          );
          return array;
        }
      )
    );
  }

  insetNewCliente(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/insetNewCliente`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  insertsInicial(ambiente: AmbienteModel ){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/insertsInicial`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  // actualizarIp(servidores: ServidoresModel){
  //   const authData = {
  //     ...servidores
  //   };
  //   return this.http.post(
  //     `${this.auth.url}ambientes/cambioIp`, authData ,
  //     {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
  //   ).pipe(
  //     map(
  //       resp => {
  //         return resp;
  //       }
  //     )
  //   );
  // }

  execProcedure(ambiente: AmbienteModel){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/execProcedure`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  envioEmail(ambiente: AmbienteModel){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/enviarEmail`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  envioEmailDestroy(ambiente: AmbienteModel){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/enviarEmailDestroy`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  validarEmpresa(ambiente: AmbienteModel){
      const authData = {
        ...ambiente
      };
      return this.http.post(
        `${this.auth.url}ambientes/validacionEmpresa`, authData ,
        {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
      ).pipe(
        map(
          resp => {
            return resp;
          }
        )
      );
  }

  validarEmpleadosLectores(ambiente: AmbienteModel){
    const authData = {
      ...ambiente
    };
    return this.http.post(
      `${this.auth.url}ambientes/validacionEmpleadosLectores`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }
}
