import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AlertasService {

  constructor(private http: HttpClient, private auth: AuthService) {

  }

  getAllAlerts() {
    let array = [];
    return this.http.get<[]>(
      `${this.auth.url}alertas/index`,
      { headers: { 'Authorization': `Bearer ${this.auth.userToken}` } },
    ).pipe(
      map(
        resp => {
          resp.forEach(
            (elem: any) => {
              /*              if (elem['password'] === null) {
                              elem['password']='';
                            }*/
              array.push(elem);
            }
          );
          return array;
        }
      )
    );
  }

  update(data) {
    let authData = {
      ...data
    };
    return this.http.post<[]>(
      `${this.auth.url}alertas/update`, authData,
      { headers: { 'Authorization': `Bearer ${this.auth.userToken}` } },
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );

  }

  save(data) {
    let authData = {
      ...data
    };
    return this.http.post<[]>(
      `${this.auth.url}alertas/save`, authData,
      { headers: { 'Authorization': `Bearer ${this.auth.userToken}` } },
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  validarServer(){
    return this.http.get<[]>(
      `${this.auth.url}alertas/validarServer`,
      { headers: { 'Authorization': `Bearer ${this.auth.userToken}` } },
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }
}
