import { Injectable } from '@angular/core';
import {AmbienteModel} from '../models/ambiente/ambiente.model';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';
import {ServidorModel} from '../models/servidor/servidor.model';

@Injectable({
  providedIn: 'root'
})
export class ServidorService {

  constructor(private http: HttpClient , private auth: AuthService) { }


  getAllServers( ){
    let array = [];
    return this.http.get<[]>(
      `${this.auth.url}server/index` ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          resp.forEach(
            (elem: any) => {
              array.push(elem);
            }
          );
          return array;
        }
      )
    );
  }

  getAllConnsByIdServer(data){
    let authData = {
      ...data
    };
    let array = [];
    return this.http.post<[]>(
      `${this.auth.url}server/getConnsByIdServer`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          resp.forEach(
            (elem: any) => {
              array.push(elem);
            }
          );
          return array;
        }
      )
    );
  }

  save( data ){
      let authData = {
        ...data
      };
      return this.http.post<[]>(
        `${this.auth.url}server/save`, authData ,
        {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
      ).pipe(
        map(
          resp => {
            return resp;
          }
        )
      );
  }

  updateServers( data ){
    let authData = {
      ...data
    };
    return this.http.post<[]>(
      `${this.auth.url}server/updateServers`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  updateConnsServer( data ){
    let authData = {
      ...data
    };
    return this.http.post<[]>(
      `${this.auth.url}server/updateConnsServer`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  deleteServer( data ){
    return this.http.post<[]>(
      `${this.auth.url}server/delete/${data}`, data ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  deletegeneral( data ){
    return this.http.post<[]>(
      `${this.auth.url}server/deletegeneral`, data ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }

  obtenerBD( data ){
    let authData = {
      ...data
    };
    return this.http.post<[]>(
      `${this.auth.url}server/dataCentraliza`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    ).pipe(
      map(
        resp => {
          return resp;
        }
      )
    );
  }


}
