export class ClientesModel{
    id:number;
    nombre:string;
    nombre_db:string;
    producto:string;
    ip_psql?:string;
    lectores?:LectorModel[];
    ultima_actualizacion?:string;
}

export class LectorModel{
    id:number;
    nombre:string;
    no_serie:string;
    modelo:string;
    ultima_conexion:string;
    estatus:number;
    marcajes_hoy:number;
    cliente:number;
}


export class LectorCronModel{
  id?: number;
  nombre?: string;
  estado: number;
  tipo_hora?: string;
  horas?: LectorCronHorasModel[];
  acciones?:any[];
}

export class LectorCronHorasModel{
  id?: number;
  sentencia?: string;
  hora?:string;
  id_table?:number;
}

export class BitacoraLector{
  id:number;
  id_cliente:string;
  lector:string;
  no_serie:string;
  problema:any[];
  observacion:any[];
  comentario_cliente?:any[];
  registro:any[];
  created_at: any[];
}
