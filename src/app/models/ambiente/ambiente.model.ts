export class AmbienteModel{
  server_name: string;
  server_2: string;
  empleadosActivos: String;
  lectoresLinea: String;
  rfc: String;
  tipoContacto1: String;
  telefono1: String;
  email_contacto1: string;
  cliente: string;
  usuario: string;
  password: string;
  codigoEmpresa: string;
  clave: String;
  descripcion: String;
  //codigoSucursal: string;
  //descSucursal: string;
  descEmpresa: string;
  name: string;
  bd_origen: string;
  bd_nueva: string;
  producto: string;
  subproducto: string;
  email: string;
  calle: string;
  cp: string;
  usuario_email: string;
  dirEmpresa: string;
  contacto: string;
  telefono: string;
  codigoTeminal:string;
  descTerminal:string;
  ipTerminal:string;
  mac:string;
  modelo:string;
}


