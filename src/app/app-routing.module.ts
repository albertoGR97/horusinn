import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from './guards/auth/auth.guard';
import {NoAuthGuard} from './guards/auth/no-auth.guard';
import {PerfilGuard} from './guards/perfil/perfil.guard';
import {ActivationComponent} from './components/pages/activation/activation.component';
import {ErrorComponent} from './components/pages/error/error.component';

const routes: Routes = [
  { path: 'login', loadChildren: () => import('./components/pages/login/login.module').then(m => m.LoginModule), canActivate: [NoAuthGuard] },
  { path: 'signup', loadChildren: () => import('./components/pages/signup/signup.module').then(m => m.SignupModule), canActivate: [NoAuthGuard] },
  { path: 'reset-key', loadChildren: () => import('./components/pages/reset-key/reset-key.module').then(m => m.ResetKeyModule),canActivate: [NoAuthGuard] },
  { path: 'reset-key-form', loadChildren: () => import('./components/pages/reset-key-form/reset-key-form.module').then(m => m.ResetKeyFormModule),canActivate: [NoAuthGuard] },
  { path: 'home', loadChildren: () => import('./components/pages/home/home.module').then(m => m.HomeModule), canActivate: [AuthGuard] },
  { path: 'usuarios', loadChildren: () => import('./components/pages/usuarios/usuarios.module').then(m => m.UsuariosModule), canActivate: [AuthGuard,PerfilGuard] },

  { path: 'girha/ambientes', loadChildren: () => import('./components/pages/ambientes/girha-ambientes/girha-ambientes.module').then(m => m.GirhaAmbientesModule), canActivate: [AuthGuard] },
  { path: 'mi_asistencia/ambientes', loadChildren: () => import('./components/pages/ambientes/mi-asistencia-ambientes/mi-asistencia-ambientes.module').then(m => m.MiAsistenciaAmbientesModule), canActivate: [AuthGuard] },
  { path: 'liber/ambientes', loadChildren: () => import('./components/pages/ambientes/liber-ambientes/liber-ambientes.module').then(m => m.LiberAmbientesModule), canActivate: [AuthGuard] },
  { path: 'biotimecloud/ambientes', loadChildren: () => import('./components/pages/ambientes/biotime-cloud-ambientes/biotime-cloud-ambientes.module').then(m => m.BiotimeCloudAmbientesModule), canActivate: [AuthGuard] },

  { path: 'servidores', loadChildren: () => import('./components/pages/servidores/servidores.module').then(m => m.ServidoresModule), canActivate: [AuthGuard,PerfilGuard] },
  { path: 'empresas/girha/girha-alertas', loadChildren: () => import('./components/pages/empresas/girha/girha-alertas/girha-alertas.module').then(m => m.GirhaAlertasModule), canActivate: [AuthGuard,PerfilGuard] },
  { path: 'lectores', loadChildren: () => import('./components/pages/lectores/lectores.module').then(m => m.LectoresModule), canActivate: [AuthGuard,PerfilGuard] },
  { path: 'activation', component: ActivationComponent},
  { path: 'configuracion/lectores', loadChildren: () => import('./components/pages/configuracion/lectores-configuracion/lectores-configuracion.module').then(m => m.LectoresConfiguracionModule), canActivate: [AuthGuard,PerfilGuard] },

  { path: 'girha/destruccion', loadChildren: () => import('./components/pages/ambientes/girha-destruccion/girha-destruccion.module').then(m => m.GirhaDestruccionModule), canActivate: [AuthGuard,PerfilGuard] },
  { path: 'biotimecloud/destruccion', loadChildren: () => import('./components/pages/ambientes/biotime-cloud-destruccion/biotime-cloud-destruccion.module').then(m => m.BioTimeCloudDestruccionModule), canActivate: [AuthGuard,PerfilGuard] },
  //modulo de administracion
  //{ path: 'administracion/administracion', loadChildren:() => import('./components/administracion/administracion.component')}
  { path: 'administracion/administracion', loadChildren: ()=> import('./components/pages/administracion/administracion.module').then(m=> m.AdministracionModule), canActivate:[AuthGuard,PerfilGuard]},
  //---fin del modulo
  { path: 'error', component: ErrorComponent},
  { path: '' , redirectTo: 'login', pathMatch: 'full' },
  { path: '**' , redirectTo: 'error', pathMatch: 'full' }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
