import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServidoresRoutingModule } from './servidores-routing.module';
import { ServidoresComponent } from './servidores.component';
import {NavigationModule} from '../../shared/navigation/navigation.module';
import {MultiSelectModule} from 'primeng/multiselect';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TableModule} from 'primeng/table';
import {MessageModule} from 'primeng/message';
import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';
import {ButtonModule} from 'primeng/button';
import {DialogModule} from 'primeng/dialog';
import {TooltipModule} from 'primeng/tooltip';
import {RippleModule} from 'primeng/ripple';
import {ToastModule} from 'primeng/toast';
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {CheckboxModule} from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import {SplitButtonModule} from 'primeng/splitbutton';
import {FileUploadModule} from 'primeng/fileupload';


@NgModule({
  declarations: [ServidoresComponent],
    imports: [
        CommonModule,
        ServidoresRoutingModule,
        NavigationModule,
        MultiSelectModule,
        FormsModule,
        TableModule,
        MessageModule,
        ReactiveFormsModule,
        InputTextModule,
        DropdownModule,
        ButtonModule,
        DialogModule,
        TooltipModule,
        RippleModule,
        ToastModule,
        NgbTooltipModule,
        CheckboxModule,
        ConfirmDialogModule,
        SplitButtonModule,
        FileUploadModule
    ]
})
export class ServidoresModule { }
