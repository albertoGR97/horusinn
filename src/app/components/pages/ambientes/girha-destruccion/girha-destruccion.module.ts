import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GirhaDestruccionRoutingModule } from './girha-destruccion-routing.module';
import { GirhaDestruccionComponent } from './girha-destruccion.component';
import {DropdownModule} from 'primeng/dropdown';
import {FormsModule} from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import {PasswordModule} from 'primeng/password';
import {TooltipModule} from 'primeng/tooltip';
import {ButtonModule} from 'primeng/button';
import {ChipsModule} from 'primeng/chips';
import {NavigationModule} from '../../../shared/navigation/navigation.module';
import {TableModule} from 'primeng/table';
import {ProgressBarModule} from 'primeng/progressbar';
import {MessagesModule} from 'primeng/messages';
import {LoadingModule} from '../../../shared/loading/loading.module';
import {MultiSelectModule} from 'primeng/multiselect';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {ToastModule} from 'primeng/toast';


@NgModule({
  declarations: [GirhaDestruccionComponent],
  imports: [
    CommonModule,
    GirhaDestruccionRoutingModule,
    DropdownModule,
    FormsModule,
    InputTextModule,
    PasswordModule,
    TooltipModule,
    ButtonModule,
    ChipsModule,
    NavigationModule,
    TableModule,
    ProgressBarModule,
    MessagesModule,
    LoadingModule,
    MultiSelectModule,
    AutoCompleteModule,
    ToastModule
  ]
})
export class GirhaDestruccionModule { }
