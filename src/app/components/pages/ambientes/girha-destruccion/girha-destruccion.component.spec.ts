import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GirhaDestruccionComponent } from './girha-destruccion.component';

describe('GirhaDestruccionComponent', () => {
  let component: GirhaDestruccionComponent;
  let fixture: ComponentFixture<GirhaDestruccionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GirhaDestruccionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GirhaDestruccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
