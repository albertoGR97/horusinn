import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GirhaAmbientesComponent } from './girha-ambientes.component';

const routes: Routes = [{ path: '', component: GirhaAmbientesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GirhaAmbientesRoutingModule { }
