import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GirhaAmbientesComponent } from './girha-ambientes.component';

describe('GirhaAmbientesComponent', () => {
  let component: GirhaAmbientesComponent;
  let fixture: ComponentFixture<GirhaAmbientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GirhaAmbientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GirhaAmbientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
