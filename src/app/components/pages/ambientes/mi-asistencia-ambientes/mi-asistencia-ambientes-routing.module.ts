import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MiAsistenciaAmbientesComponent } from './mi-asistencia-ambientes.component';

const routes: Routes = [{ path: '', component: MiAsistenciaAmbientesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MiAsistenciaAmbientesRoutingModule { }
