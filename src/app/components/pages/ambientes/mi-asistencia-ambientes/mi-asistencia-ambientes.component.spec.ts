import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiAsistenciaAmbientesComponent } from './mi-asistencia-ambientes.component';

describe('MiAsistenciaAmbientesComponent', () => {
  let component: MiAsistenciaAmbientesComponent;
  let fixture: ComponentFixture<MiAsistenciaAmbientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiAsistenciaAmbientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiAsistenciaAmbientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
