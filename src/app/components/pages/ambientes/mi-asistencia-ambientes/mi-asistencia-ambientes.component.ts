import { Component, HostListener, OnInit } from '@angular/core';
import { AmbienteModel } from '../../../../models/ambiente/ambiente.model';
import { LogModel } from '../../../../models/log/log.model';
import { AmbientesService } from '../../../../services/ambientes.service';
import { LogService } from '../../../../services/log.service';
import { NgForm } from '@angular/forms';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-mi-asistencia-ambientes',
  templateUrl: './mi-asistencia-ambientes.component.html',
  styleUrls: ['./mi-asistencia-ambientes.component.css'],
  providers: [MessageService]
})
export class MiAsistenciaAmbientesComponent implements OnInit {

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event) {
    if (this.verificado && !this.completado && !this.correo || !this.verificado && this.completado && !this.correo || this.verificado && this.completado && !this.correo) {
      console.log(event);
      event.preventDefault();
      event.returnValue = false;
      event.stopPropagation();
    }
  }

  ambiente: AmbienteModel;
  log_ambiente: LogModel;
  tables: any;
  funciones: any;
  events: any;
  sp: any;
  triggers: any;
  views: any;
  tareas: any;
  extras: any;
  verificado = false;
  progressPorcent = 0;
  total: number;
  numEjecutados = 0;
  msgs1 = [];
  detail = []
  correos = []
  detalles: any;
  empresaError = true;
  validado = false;
  cargando = false;
  completado = false;
  correo = false;
  enviado = false;
  error = false;
  errorGral = false;
  seleccionado = false;
  limpiar = false;
  bloqueo = false;

  constructor(private ambientesService: AmbientesService, private logService: LogService) { }

  ngOnInit(): void {
    this.ambiente = new AmbienteModel();
    this.log_ambiente = new LogModel();
    this.ambiente.producto = 'mi_asistencia'
    this.log_ambiente.producto = 'mi_asistencia';
    this.getServidores();
    this.logService.obtenerUsuario();
  }

  //funcion principal
  async nuevo(form: NgForm, tipo) {
    if (!form.invalid && !this.empresaError && this.ambiente.cliente !== undefined) {
      if (tipo === 1) {
        this.msgs1 = [];
        this.detail = [];
        this.cargando = true;
        //obtiene todos los elementos para la BD del cliente
        this.ambientesService.getTablesName(this.ambiente).subscribe(
          resp => {
            this.tables = resp;
            this.ambientesService.getFunciones(this.ambiente).subscribe(
              funciones => {
                this.funciones = funciones;
                this.ambientesService.getEvents(this.ambiente).subscribe(
                  events => {
                    this.events = events;
                    this.ambientesService.getSP(this.ambiente).subscribe(
                      sp => {
                        this.sp = sp;
                        this.ambientesService.getTriggers(this.ambiente).subscribe(
                          triggers => {
                            this.triggers = triggers;
                            this.ambientesService.getViews(this.ambiente).subscribe(
                              views => {
                                this.views = views;
                                this.verificado = true;
                                this.cargando = false;
                                this.total = this.tables.length + this.funciones.length + this.events.length +
                                  + this.sp.length + this.triggers.length + this.views.length;
                                this.igualaArray();
                              }, error => this.errorHandler(error, 1));
                          }, error => this.errorHandler(error, 1));
                      }, error => this.errorHandler(error, 1));
                  }, error => this.errorHandler(error, 1));
              }, error => this.errorHandler(error, 1));
          }, error => this.errorHandler(error, 1));
        this.crearLog();
      }
      else if (tipo === 2) {

        this.bloqueo = true;
        if (this.ambiente.bd_nueva === undefined) {
          await new Promise((resolve, reject) => {
            //solicita crear la BD para el cliente
            this.log_ambiente.campo = 'base';
            this.actualizarEstadoLog(0);
            this.ambiente.name = 'base de datos'
            this.ambientesService.creaBase(this.ambiente).subscribe(
              async base => {
                this.actualizarEstadoLog(1);
                this.ambiente.bd_nueva = base['name'];
                resolve();
              }, error => this.errorHandler(error, 2, null, this.ambiente.name));
          });
        }
        this.error = false;
        this.msgs1 = [];
        this.detail = [];
        this.log_ambiente.campo = 'tablas';
        this.actualizarEstadoLog(0);
        for (const elem of this.tables) {
          if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
            this.ambiente.name = elem.Tables_name;
            await new Promise((resolve, reject) => {
              //solicita crear las tablas y las vistas las crea con info temporal 
              this.ambientesService.creaTablas(this.ambiente)
                .subscribe(resp => {
                  elem.ejecutado = 1;
                  this.actualizaProgress();
                  resolve();
                  /* setTimeout(() => { resolve(); }, 500);*/
                }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
            });
          }
        }
        this.actualizarEstadoLog(1);
        this.log_ambiente.campo = 'funciones';
        this.actualizarEstadoLog(0);
        for (const elem of this.funciones) {
          if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
            this.ambiente.name = elem.Name;
            await new Promise((resolve, reject) => {
              //solicita crear las funciones
              this.ambientesService.creaFunciones(this.ambiente)
                .subscribe(resp => {
                  elem.ejecutado = 1;
                  this.actualizaProgress();
                  resolve();
                  /* setTimeout(() => { resolve(); }, 500);*/
                }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
            });
          }
        }
        this.actualizarEstadoLog(1);
        this.log_ambiente.campo = 'eventos';
        this.actualizarEstadoLog(0);
        for (const elem of this.events) {
          if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
            this.ambiente.name = elem.Name;
            await new Promise((resolve, reject) => {
              //solicita crear los eventos
              this.ambientesService.creaEvents(this.ambiente)
                .subscribe(resp => {
                  elem.ejecutado = 1;
                  this.actualizaProgress();
                  resolve();
                  /* setTimeout(() => { resolve(); }, 500);*/
                }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
            });
          }
        }
        this.actualizarEstadoLog(1);
        this.log_ambiente.campo = 'sp';
        this.actualizarEstadoLog(0);
        for (const elem of this.sp) {
          if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
            this.ambiente.name = elem.Name;
            await new Promise((resolve, reject) => {
              //solicita crear los SP
              this.ambientesService.creaSP(this.ambiente)
                .subscribe(resp => {
                  elem.ejecutado = 1;
                  this.actualizaProgress();
                  resolve();
                  /* setTimeout(() => { resolve(); }, 500);*/
                }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
            });
          }
        }
        this.actualizarEstadoLog(1);
        this.log_ambiente.campo = 'triggers';
        this.actualizarEstadoLog(0);
        for (const elem of this.triggers) {
          if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
            this.ambiente.name = elem.Trigger;
            await new Promise((resolve, reject) => {
              //solicita crear los triggers
              this.ambientesService.creaTriggers(this.ambiente)
                .subscribe(resp => {
                  elem.ejecutado = 1;
                  this.actualizaProgress();
                  resolve();
                  /* setTimeout(() => { resolve(); }, 500);*/
                }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
            });
          }
        }
        this.actualizarEstadoLog(1);
        this.log_ambiente.campo = 'views';
        this.actualizarEstadoLog(0);
        for (const elem of this.views) {
          if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
            this.ambiente.name = elem.Tables_name;
            await new Promise((resolve, reject) => {
              //solicita crear las vistas
              this.ambientesService.creaViews(this.ambiente)
                .subscribe(resp => {
                  elem.ejecutado = 1;
                  this.actualizaProgress();
                  resolve();
                  /* setTimeout(() => { resolve(); }, 500);*/
                }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
            });
          }
        }
        this.actualizarEstadoLog(1);
        this.completado = true;
        this.verificado = false;
        this.bloqueo = false;
        this.msgs1 = [{ severity: 'success', summary: 'Completado!', detail: 'La base de datos del ambiente se ha creado' }];
      }
      else if (tipo === 3) {
        this.error = false;
        this.msgs1 = [];
        this.detail = [];
        this.cargando = true;
        //obtiene tareas adicionales que se requieren
        this.ambientesService.getTareasAdicionales(this.ambiente).subscribe(
          tareas => {
            this.tareas = tareas;
            this.verificado = true;
            this.cargando = false;
            this.total = this.tareas.length;
            this.numEjecutados = 0;
            this.progressPorcent = 0;
            this.msgs1 = [];
            this.detail = [];
          }, error => this.errorHandler(error, 1));
      }
      else if (tipo === 4) {
        this.bloqueo = true;
        this.log_ambiente.campo = 'tareas';
        this.actualizarEstadoLog(0);
        this.error = false;
        this.msgs1 = [];
        this.detail = [];
        for (const elem of this.tareas) {
          if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
            this.ambiente.name = elem.tabla;
            await new Promise((resolve, reject) => {
              //define informacion default
              this.ambientesService.creaInformacionDefault(this.ambiente)
                .subscribe(resp => {
                  elem.ejecutado = 1;
                  this.actualizaProgress();
                  resolve();
                }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
            });
          }
        }
        this.cargando = true;
        this.correo = true;
        this.cargando = false;
        this.extras = [{ Name: 'enviar_correo', ejecutado: 0, }];
        this.enviado = false;
        this.total = this.extras.length;
        this.numEjecutados = 0;
        this.progressPorcent = 0;
        this.bloqueo = false;
        this.msgs1 = [{ severity: 'success', summary: 'Casi completo!', detail: 'Escriba el email notificación' }];
        this.actualizarEstadoLog(1);
      } else if (tipo === 5 && (this.ambiente.email != null || this.ambiente.email != '')) {
        this.error = false
        this.bloqueo = true;
        this.msgs1 = [];
        this.detail = [];
        this.ambiente.name = "insertar cliente";
        this.log_ambiente.campo = 'extras';
        this.actualizarEstadoLog(0);
        await new Promise((resolve, reject) => {
          //inserta al nuevo cliente
          this.ambientesService.insetNewCliente(this.ambiente)
            .subscribe(resp => {
              resolve();
            }, error => this.errorHandler(error, 2, null, this.ambiente.name));
        });
        for (const elem of this.extras) {
          this.ambiente.name = elem.Name
          if (elem.Name != 'enviar_correo') {
            await new Promise((resolve, reject) => {
              this.ambientesService.execProcedure(this.ambiente).subscribe(
                //ejecuta el SP sp_actualiza_ip_clientes
                resp => {
                  elem.ejecutado = 1;
                  this.actualizaProgress();
                  resolve();
                }, error => this.errorHandler(error, 2, this.ambiente.name)
              );
            });
          } else {
            await new Promise((resolve, reject) => {
              //solicitud de correo
              this.ambientesService.envioEmail(this.ambiente).subscribe(
                resp => {
                  elem.ejecutado = 1;
                  this.enviado = true;
                  this.bloqueo = false;
                  this.msgs1 = [{ severity: 'success', summary: 'Completado!', detail: 'El ambiente esta listo!' }];
                  this.actualizaProgress();
                  resolve();
                }, error => this.errorHandler(error, 2, elem, this.ambiente.name)
              );
            });
          }
        }
        this.actualizarEstadoLog(1);
      } else {
        this.msgs1 = [{ severity: 'error', summary: 'Datos incompletos!', detail: 'Favor de verificar los campos!' }];
      }
    } else {
      this.msgs1 = [{ severity: 'error', summary: 'Datos Erroneos!', detail: 'Favor de verificar los campos!' }];
    }
  }

  //reinicia valores
  reiniciarValores() {
    window.location.reload();
  }

  generarPass() {
    this.ambiente.password = Math.random().toString(36).substr(2, 10);
  }

  //funcion para determinar el totaal para la funcion de la barra de progreso
  igualaArray() {
    let numero = Math.max(this.tables.length, this.funciones.length, this.events.length,
      this.sp.length, this.triggers.length, this.views.length);
    let todos = [];
    todos.push(this.tables, this.funciones, this.events, this.sp, this.triggers, this.views);
    for (const elem of todos) {
      if (elem.length > 0 && elem.length < numero) {
        while (elem.length < numero) {
          elem.push({ ejecutado: -1 });
        }
      }
    }
  }

  //funcion para mostrar el avance en la barra de progreso
  actualizaProgress() {
    this.numEjecutados++;
    this.progressPorcent = +((this.numEjecutados / this.total) * 100).toFixed(2);
  }

  agregarCorreo() {
    this.msgs1 = [];
    let fallidos = 0;
    this.correos = this.correos.filter(elem=>{
      if(this.validateEmail(elem)){
        return true;
      }else{
        fallidos ++;
      }
    });
    this.listaCorreos();
    console.log(this.correos);
    console.log(this.ambiente.email);
    if(fallidos > 0){
      this.msgs1 = [{ severity: 'error', summary: 'Error!', detail: 'Ingrese un email valido, por favor verificar y reintentar.' }];
    }
  }

  listaCorreos() {
    this.ambiente.email = this.correos.join(';');
    console.log(this.ambiente.email);
    console.log(this.correos);
  }

  //funciones para obtener los servers aplicativos y engine
  async getServidores() {
    await new Promise((resolve, reject) => {
      this.ambientesService.getServerbyProduct(this.ambiente)
        .subscribe(resp => {
          if (resp.length > 0) {
            this.errorGral = true;
            this.ambiente.server_name = resp[0]['nombre'];
            this.log_ambiente.server_apli_id = resp[0]['id'];
            this.log_ambiente.server_engine_id = 0;
          } else {
            this.errorGral = false;
          }
          resolve();
        }, error => this.errorHandler(error, 1));
    });
  }

  producto(tipo) {
    switch (tipo) {
      case 1:
        this.ambiente.subproducto = 'mi_asistencia';
        break;
      case 2:
        this.ambiente.subproducto = 'mi_asistencia_hik';
        break;
      case 3:
        this.ambiente.subproducto = 'cloudclock';
        break;
      case 4:
        this.ambiente.subproducto = 'mi_asistencia';
        this.ambiente.bd_origen = '2';
        break;
    }
    this.acuatizaCliente();
    this.seleccionado = true;
  }

  //funcion para actualizar el numero de cliente
  private acuatizaCliente() {
    this.ambientesService.getLastCliente(this.ambiente)
      .subscribe(resp => {
        this.ambiente.cliente = resp[0].total + 1;
        console.log(this.ambiente);
        this.log_ambiente.cliente = this.ambiente.cliente;
        console.log(this.log_ambiente);
      }, error => this.errorHandler(error, 1));
  }

  //datos para log
  private crearLog() {
    this.log_ambiente.user_id = sessionStorage.getItem('id_user');
    this.logService.crearLog(this.log_ambiente).subscribe(
      resp => {
        this.log_ambiente.log_id = resp['id_log'];
      },
      error => this.errorHandler(error, 2, null, 'Servidor')
    );
  }

  //actualiza el estado del log
  private actualizarEstadoLog(status_campo, error = null) {
    if (this.log_ambiente.log_id != undefined) {
      this.log_ambiente.status_campo = status_campo;
      if (error != null) {
        this.log_ambiente.error = error;
      }
      this.logService.actualizarCampo(this.log_ambiente).subscribe(
        resp => {
          console.log(resp);
        },
        error=>{
          this.msgs1 = [{ severity: 'error', summary: 'Error!', detail: 'Error al Actualizar el log, favor de contactar a soporte.' }];
        }
      );
    }
  }

  //valida el formato del correo
  validateEmail(email): boolean {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  //valida si la desc social (empresa) y rfc ya existe en la base
  async validarEmpresa() {
    this.msgs1 = [];
    this.error = false;
    if (this.ambiente.descEmpresa !== undefined && this.ambiente.codigoEmpresa !== undefined) {
      await new Promise((resolve, reject) => {
        this.ambientesService.validarEmpresa(this.ambiente)
          .subscribe(resp => {
            console.log(resp);
            this.empresaError = false;
            this.validado = true;
            this.detalles = undefined;
            resolve();
          }, error => {
            console.log(error);
            this.empresaError = true;
            this.detalles = error.error.error;
            
          });
      });
    } else {
      this.msgs1 = [{ severity: 'error', summary: 'Datos Incompletos!', detail: 'Favor de verificar los campos!' }]
    }
  }

  //errores
  //manejador de errores
  errorHandler(error, tipo, elem = null, nombre = null) {
    this.bloqueo = false;
    this.detalles = undefined;
    if (tipo === 1) {
      console.log(error);
      this.cargando = false;
      this.detalles = 'Error en el cliente';
      this.msgs1 = [{ severity: 'error', summary: 'Error!', detail: 'Error inesperado, favor de intentarlo más tarde.' }];
    } else if (tipo === 2) {
      console.log(error);
      if (elem != null) {
        elem.ejecutado = 2;
        elem.error = error.error.message;
      }
      this.error = true;
      if(error.error.error != undefined){
        this.detalles = error.error.error;
      } else {
        this.detalles = error.error.message;
      }
      this.msgs1 = [{ severity: 'error', summary: 'Error!', detail: 'Ocurrió un error en ' + nombre + ', por favor verificar y reintentar.' }];
      this.actualizarEstadoLog(2, this.detalles);
    }
    
  }
  
  //funcion para mostrar los detalles de un error tipo 2
  detallesMsg() {
    this.detail=[{ severity: 'info', summary: 'Error!', detail: this.detalles }];
    this.detalles=undefined;
  }
}
