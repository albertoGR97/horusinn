import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BioTimeCloudAmbientesComponent } from './biotime-cloud-ambientes.component';

const routes: Routes = [{ path: '', component: BioTimeCloudAmbientesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BiotimeCloudAmbientesRoutingModule { }
