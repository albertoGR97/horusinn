import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LiberAmbientesRoutingModule } from './liber-ambientes-routing.module';
import { LiberAmbientesComponent } from './liber-ambientes.component';
import {NavigationModule} from '../../../shared/navigation/navigation.module';
import {InputTextModule} from 'primeng/inputtext';
import {PasswordModule} from 'primeng/password';
import {FormsModule} from '@angular/forms';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {ProgressBarModule} from 'primeng/progressbar';
import {LoadingModule} from '../../../shared/loading/loading.module';
import {MessagesModule} from 'primeng/messages';
import {DropdownModule} from 'primeng/dropdown';
import {TooltipModule} from 'primeng/tooltip';
import { ChipsModule } from 'primeng/chips';


@NgModule({
  declarations: [LiberAmbientesComponent],
  imports: [
    CommonModule,
    LiberAmbientesRoutingModule,
    NavigationModule,
    InputTextModule,
    PasswordModule,
    FormsModule,
    ButtonModule,
    TableModule,
    ProgressBarModule,
    LoadingModule,
    MessagesModule,
    DropdownModule,
    TooltipModule,
    ChipsModule
  ]
})
export class LiberAmbientesModule { }
