import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LiberAmbientesComponent } from './liber-ambientes.component';

const routes: Routes = [{ path: '', component: LiberAmbientesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LiberAmbientesRoutingModule { }
