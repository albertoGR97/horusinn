import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiberAmbientesComponent } from './liber-ambientes.component';

describe('LiberAmbientesComponent', () => {
  let component: LiberAmbientesComponent;
  let fixture: ComponentFixture<LiberAmbientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiberAmbientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiberAmbientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
