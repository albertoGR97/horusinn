import { Component, HostListener, OnInit } from '@angular/core';
import { AmbienteModel } from '../../../../models/ambiente/ambiente.model';
import { NgForm } from '@angular/forms';
import { AmbientesService } from '../../../../services/ambientes.service';
import { MessageService } from 'primeng/api';
import { LogModel } from 'src/app/models/log/log.model';
import { LogService } from 'src/app/services/log.service';

@Component({
  selector: 'app-liber-ambientes',
  templateUrl: './liber-ambientes.component.html',
  styleUrls: ['./liber-ambientes.component.css'],
  providers: [MessageService]
})

export class LiberAmbientesComponent implements OnInit {


  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event) {
    if (this.verificado && !this.completado && !this.correo || !this.verificado && this.completado && !this.correo || this.verificado && this.completado && !this.correo) {
      console.log(event);
      // failed attempts to prevent popup =>
      event.preventDefault();
      event.returnValue = false;
      event.stopPropagation();
    }
  }

  ambiente: AmbienteModel;
  log_ambiente: LogModel;
  servers: any;
  tables: any;
  funciones: any;
  events: any;
  sp: any;
  triggers: any;
  views: any;
  tareas: any;
  extras: any;
  verificado = false;
  progressPorcent = 0;
  total: number;
  numEjecutados = 0;
  msgs1 = [];
  detail = []
  correos = []
  detalles: any;
  empresaError = false;
  errorGral = true;
  cargando = false;
  completado = false;
  correo = false;
  enviado = false;
  error = false;
  limpiar = false;
  bloqueo = false;
  base1 = false;
  base2 = false;
  tareasEjecutadas = false;

  constructor(private ambientesService: AmbientesService, private logService: LogService) { }


  ngOnInit(): void {
    this.ambiente = new AmbienteModel();
    this.log_ambiente = new LogModel();
    this.ambiente.producto = 'liber';
    this.getServidores();
    this.logService.obtenerUsuario();
  }

  async nuevo(form: NgForm, tipo) {
    if (!form.invalid) {
      if (tipo === 1 && !this.empresaError ) {
        this.msgs1 = [];
        this.detail = [];
        if (this.log_ambiente.log_id === undefined) {
          this.crearLog();
          this.log_ambiente.campo = 'base';
          this.cargando = true;
          this.getElementosBase()
        }
      } else if (tipo === 2 && this.log_ambiente.log_id !== undefined) {
        this.error = false;
        this.msgs1 = [];
        this.detail = [];
        this.bloqueo = true;

        if (this.log_ambiente.campo == 'base' && this.error == false) {
          await new Promise((resolve, reject) => {
            //solicita crear la BD para el cliente
            this.actualizarEstadoLog(0);
            this.ambiente.name = 'base de datos';
            this.ambientesService.creaBase(this.ambiente).subscribe(
              async base => {
                this.actualizarEstadoLog(1);
                this.ambiente.bd_nueva = base['name'];
                resolve();
              }, error => this.errorHandler(error, 2, null, this.ambiente.name));
          });
          this.log_ambiente.campo = 'tablas';
        }

        if (this.log_ambiente.campo == 'tablas' && this.error == false) {
          this.actualizarEstadoLog(0);
          for (const elem of this.tables) {
            if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
              this.ambiente.name = elem.Tables_name;
              await new Promise((resolve, reject) => {
                //solicita crear las tablas y las vistas las crea con info temporal 
                this.ambientesService.creaTablas(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress();
                    resolve();
                    /* setTimeout(() => { resolve(); }, 500);*/
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            }
          }
          this.actualizarEstadoLog(1);
          this.log_ambiente.campo = 'funciones';
        }

        if (this.log_ambiente.campo == 'funciones' && this.error == false) {
          this.actualizarEstadoLog(0);
          for (const elem of this.funciones) {
            if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
              this.ambiente.name = elem.Name;
              await new Promise((resolve, reject) => {
                //solicita crear las funciones
                this.ambientesService.creaFunciones(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress();
                    resolve();
                    /* setTimeout(() => { resolve(); }, 500);*/
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            }
          }
          this.actualizarEstadoLog(1);
          this.log_ambiente.campo = 'eventos';
        }

        if (this.log_ambiente.campo == 'eventos' && this.error == false) {
          this.actualizarEstadoLog(0);
          for (const elem of this.events) {
            if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
              this.ambiente.name = elem.Name;
              await new Promise((resolve, reject) => {
                //solicita crear los eventos
                this.ambientesService.creaEvents(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress();
                    resolve();
                    /* setTimeout(() => { resolve(); }, 500);*/
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            }
          }
          this.actualizarEstadoLog(1);
          this.log_ambiente.campo = 'sp';
        }

        if (this.log_ambiente.campo == 'sp' && this.error == false) {
          this.actualizarEstadoLog(0);
          for (const elem of this.sp) {
            if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
              this.ambiente.name = elem.Name;
              await new Promise((resolve, reject) => {
                //solicita crear los SP
                this.ambientesService.creaSP(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress();
                    resolve();
                    /* setTimeout(() => { resolve(); }, 500);*/
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            }
          }
          this.actualizarEstadoLog(1);
          this.log_ambiente.campo = 'triggers';
        }

        if (this.log_ambiente.campo == 'triggers' && this.error == false) {
          this.actualizarEstadoLog(0);
          for (const elem of this.triggers) {
            if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
              this.ambiente.name = elem.Trigger;
              await new Promise((resolve, reject) => {
                //solicita crear los triggers
                this.ambientesService.creaTriggers(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress();
                    resolve();
                    /* setTimeout(() => { resolve(); }, 500);*/
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            }
          }
          this.actualizarEstadoLog(1);
          this.log_ambiente.campo = 'views';
        }

        if (this.log_ambiente.campo == 'views' && this.error == false) {
          this.actualizarEstadoLog(0);
          for (const elem of this.views) {
            if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
              this.ambiente.name = elem.Tables_name;
              await new Promise((resolve, reject) => {
                //solicita crear las vistas
                this.ambientesService.creaViews(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress();
                    resolve();
                    /* setTimeout(() => { resolve(); }, 500);*/
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            }
          }
          this.actualizarEstadoLog(1);
        }
        this.base1 = true;
        this.bloqueo=false;
        this.msgs1 = [{ severity: 'success', summary: 'Completado!', detail: 'La base de datos 1 del ambiente se ha creado' }];

      } else if (tipo === 3) {
        this.ambiente.bd_origen = '_historico';
        this.log_ambiente.campo = 'tareas';
        this.actualizarEstadoLog(0);
        this.msgs1 = [];
        this.detail = [];
        this.cargando = true;
        this.getElementosBase();
        this.base2 = true;
        this.log_ambiente.campo = 'base';

      } else if (tipo === 4) {
        this.bloqueo = true;
        this.error = false;
        this.msgs1 = [];
        this.detail = [];

        if (this.log_ambiente.campo == 'base' && this.error == false) {
          this.creaBase();
          this.log_ambiente.campo = 'tablas';
        }

        if (this.log_ambiente.campo == 'tablas' && this.error == false) {
          for (const elem of this.tables) {
            if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
              this.ambiente.name = elem.Tables_name;
              await new Promise((resolve, reject) => {
                //solicita crear las tablas y las vistas las crea con info temporal 
                this.ambientesService.creaTablas(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress();
                    resolve();
                    /* setTimeout(() => { resolve(); }, 500);*/
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            }
          }
          this.log_ambiente.campo = 'funciones';
        }

        if (this.log_ambiente.campo == 'funciones' && this.error == false) {
          for (const elem of this.funciones) {
            if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
              this.ambiente.name = elem.Name;
              await new Promise((resolve, reject) => {
                //solicita crear las funciones
                this.ambientesService.creaFunciones(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress();
                    resolve();
                    /* setTimeout(() => { resolve(); }, 500);*/
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            }
          }
          
          this.log_ambiente.campo = 'eventos';
        }

        if (this.log_ambiente.campo == 'eventos' && this.error == false) {
          
          for (const elem of this.events) {
            if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
              this.ambiente.name = elem.Name;
              await new Promise((resolve, reject) => {
                //solicita crear los eventos
                this.ambientesService.creaEvents(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress();
                    resolve();
                    /* setTimeout(() => { resolve(); }, 500);*/
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            }
          }
          
          this.log_ambiente.campo = 'sp';
        }

        if (this.log_ambiente.campo == 'sp' && this.error == false) {
          
          for (const elem of this.sp) {
            if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
              this.ambiente.name = elem.Name;
              await new Promise((resolve, reject) => {
                //solicita crear los SP
                this.ambientesService.creaSP(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress();
                    resolve();
                    /* setTimeout(() => { resolve(); }, 500);*/
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            }
          }
          
          this.log_ambiente.campo = 'triggers';
        }

        if (this.log_ambiente.campo == 'triggers' && this.error == false) {
          
          for (const elem of this.triggers) {
            if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
              this.ambiente.name = elem.Trigger;
              await new Promise((resolve, reject) => {
                //solicita crear los triggers
                this.ambientesService.creaTriggers(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress();
                    resolve();
                    /* setTimeout(() => { resolve(); }, 500);*/
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            }
          }
          
          this.log_ambiente.campo = 'views';
        }

        if (this.log_ambiente.campo == 'views' && this.error == false) {
          
          for (const elem of this.views) {
            if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
              this.ambiente.name = elem.Tables_name;
              await new Promise((resolve, reject) => {
                //solicita crear las vistas
                this.ambientesService.creaViews(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress();
                    resolve();
                    /* setTimeout(() => { resolve(); }, 500);*/
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            }
          }
          
        }
        this.completado = true;
        this.verificado = false;
        this.bloqueo = false;
        this.msgs1 = [{ severity: 'success', summary: 'Completado!', detail: 'La base de datos 2 del ambiente se ha creado' }];

      }
      else if (tipo === 5) {
        this.cargando = true;
        this.verificado = true;
        this.tareas = [{ nombre: 'Nuevo_cliente', ejecutado: 0 }, { nombre: 'Insert_Inicial', ejecutado: 0 }];
        this.cargando = false;
        this.total = this.tareas.length;
        this.numEjecutados = 0;
        this.progressPorcent = 0;
        this.msgs1 = [];
        this.detail = [];
      }
      else if (tipo === 6) {
        this.bloqueo = true;
        this.error = false;
        this.msgs1 = [];
        this.detail = [];
        for (const elem of this.tareas) {
          if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
            if (elem.nombre == "Nuevo_cliente") {
              this.cargando = true;
              await new Promise((resolve, reject) => {
                this.ambiente.name = elem.nombre;
                this.ambientesService.insetNewCliente(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress()
                    resolve();
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            } else if (elem.nombre == "Insert_Inicial") {
              await new Promise((resolve, reject) => {
                this.ambiente.name = elem.nombre;
                this.ambientesService.insertsInicial(this.ambiente)
                  .subscribe(resp => {
                    elem.ejecutado = 1;
                    this.actualizaProgress();
                    this.cargando = false;
                    this.bloqueo = false;
                    resolve();
                  }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
              });
            }
          }
        }
        this.cargando = false
        this.correo = true;
        this.extras = [{ Name: 'enviar_correo', ejecutado: 0 }];
        this.total = this.extras.length;
        this.numEjecutados = 0;
        this.progressPorcent = 0;
        this.bloqueo = false;
        this.msgs1 = [{ severity: 'success', summary: 'Casi completo!', detail: 'Escriba el email notificación' }];
        this.log_ambiente.campo = "tareas";
        this.actualizarEstadoLog(1);
        this.tareasEjecutadas = true;

      } else if (tipo === 7 && this.ambiente.email != null) {
        this.bloqueo = true;
        this.msgs1 = [];
        this.detail = [];
        this.log_ambiente.campo = 'extras';
        this.actualizarEstadoLog(0);

        for (const elem of this.extras) {
          this.ambiente.name = elem.Name
          await new Promise((resolve, reject) => {
            //solicitud de correo
            this.ambientesService.envioEmail(this.ambiente).subscribe(
              resp => {
                elem.ejecutado = 1;
                this.enviado = true;
                this.bloqueo = false;
                this.msgs1 = [{ severity: 'success', summary: 'Completado!', detail: 'El ambiente esta listo!' }];
                this.actualizaProgress();
                resolve();
              }, error => this.errorHandler(error, 2, null, this.ambiente.name)
            );
          });
        }
        this.actualizarEstadoLog(1);
      } else {
        this.msgs1 = [{ severity: 'error', summary: 'Datos incompletos!', detail: 'Favor de verificar los campos!' }];
      }
    } else {
      this.msgs1 = [{ severity: 'error', summary: 'Datos incompletos!', detail: 'Favor de verificar los campos!' }];

    }


  }

  reiniciarValores() {
    window.location.reload();
  }

  generarPass() {
    this.ambiente.password = Math.random().toString(36).substr(2, 10);
  }

  igualaArray() {
    let numero = Math.max(this.tables.length, this.funciones.length, this.events.length,
      this.sp.length, this.triggers.length, this.views.length);
    let todos = [];
    todos.push(this.tables, this.funciones, this.events, this.sp, this.triggers, this.views);
    for (const elem of todos) {
      if (elem.length > 0 && elem.length < numero) {
        while (elem.length < numero) {
          elem.push({ ejecutado: -1 });
        }
      }
    }
  }

  actualizaProgress() {
    this.numEjecutados++;
    this.progressPorcent = +((this.numEjecutados / this.total) * 100).toFixed(2);
  }

  agregarCorreo() {
    this.msgs1 = [];
    let fallidos = 0;
    this.correos = this.correos.filter(elem=>{
      if(this.validateEmail(elem)){
        return true;
      }else{
        fallidos ++;
      }
    });
    this.listaCorreos();
    console.log(this.correos);
    console.log(this.ambiente.email);
    if(fallidos > 0){
      this.msgs1 = [{ severity: 'error', summary: 'Error!', detail: 'Ingrese un email valido, por favor verificar y reintentar.' }];
    }
  }

  listaCorreos() {
    this.ambiente.email = this.correos.join(';');
    console.log(this.ambiente.email);
    console.log(this.correos);
  }

  async getServidores() {
    await new Promise((resolve, reject) => {
      this.ambientesService.getServerbyProduct(this.ambiente)
        .subscribe(resp => {
          this.servers = resp;
          if(this.servers.length > 0){
            this.errorGral = false;
          } else {
            this.errorGral = true;
          }
          resolve();
        }, error => this.errorHandler(error, 1));
    });
  }

  async acuatizaCliente(nombre) {
    this.ambiente.server_name = nombre;
    this.ambiente.cliente = '';
    if (nombre !== null) {
      await new Promise((resolve, reject) => {
        this.ambientesService.getLastCliente(this.ambiente)
          .subscribe(resp => {
            this.ambiente.cliente = resp[0].total + 1;
            resolve();
          }, error => this.errorHandler(error, 1));
      });
    }
  }

  async getElementosBase() {
    this.ambientesService.getTablesName(this.ambiente).subscribe(
      resp => {
        this.tables = resp;
        this.ambientesService.getFunciones(this.ambiente).subscribe(
          funciones => {
            this.funciones = funciones;
            this.ambientesService.getEvents(this.ambiente).subscribe(
              events => {
                this.events = events;
                this.ambientesService.getSP(this.ambiente).subscribe(
                  sp => {
                    this.sp = sp;
                    this.ambientesService.getTriggers(this.ambiente).subscribe(
                      triggers => {
                        this.triggers = triggers;
                        this.ambientesService.getViews(this.ambiente).subscribe(
                          views => {
                            this.views = views;
                            this.verificado = true;
                            this.cargando = false;
                            this.total = this.tables.length + this.funciones.length + this.events.length +
                              + this.sp.length + this.triggers.length + this.views.length;
                            this.igualaArray();
                            this.numEjecutados = 0;
                            this.progressPorcent = 0;
                          }, error => this.errorHandler(error, 1));
                      }, error => this.errorHandler(error, 1));
                  }, error => this.errorHandler(error, 1));
              }, error => this.errorHandler(error, 1));
          }, error => this.errorHandler(error, 1));
      }, error => this.errorHandler(error, 1));
  }

  //datos para log
  async crearLog() {
    this.log_ambiente.user_id = sessionStorage.getItem('id_user');
    this.log_ambiente.server_engine_id = 0;
    this.log_ambiente.producto = this.ambiente.producto;
    this.log_ambiente.cliente = this.ambiente.cliente;
    if (this.log_ambiente.user_id != null) {
      await new Promise((resolve, reject) => {
        this.logService.crearLog(this.log_ambiente).subscribe(
          resp => {
            this.log_ambiente.log_id = resp['id_log'];
            console.log(this.log_ambiente)
            resolve();
          },
          error => this.errorHandler(error, 2, null, 'Crear log')
        );
      });
    } else {
      this.error = true;
      this.msgs1 = [{ severity: 'error', summary: 'Error!', detail: 'Ocurrió un error en Crear Log, favor de recargar la pagina.' }];
      this.detalles = "error al buscar el id del usuario actual";
    }
  }

  private actualizarEstadoLog(status_campo, error = null) {
    this.log_ambiente.status_campo = status_campo;
    if (error != null) {
      this.log_ambiente.error = error;
    }
    this.logService.actualizarCampo(this.log_ambiente).subscribe(
      resp => {
        console.log(resp);
      },
      error=>{
        this.msgs1 = [{ severity: 'error', summary: 'Error!', detail: 'Error al Actualizar el log, favor de contactar a soporte.' }];
      }
    );
  }

  //crea base
  private creaBase() {
    this.ambientesService.creaBase(this.ambiente).subscribe(
      async base => {
        this.ambiente.bd_nueva = base['name'];
      }, error=>{
        this.msgs1 = [{ severity: 'error', summary: 'Error!', detail: 'Error al Crear el log, favor de contactar a soporte.' }];
      });
  }

  //validaciones
  //valida el formato del correo
  validateEmail(email): boolean {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  //valida si la desc social (empresa) ya existe en la base
  async validarEmpresa() {
    if (this.ambiente.descEmpresa !== null) {
      await new Promise((resolve, reject) => {
        this.ambientesService.validarEmpresa(this.ambiente)
          .subscribe(resp => {
            console.log(resp);
            this.empresaError = false;
            this.detalles = undefined;
            resolve();
          }, error => {
            console.log(error);
            this.empresaError = true;
            this.detalles = error.error.error;
          });
      });
    }
  }

  //errores
  //manejador de errores
  errorHandler(error, tipo, elem = null, nombre = null) {
    this.bloqueo=false;
    this.detalles=undefined;
    if (tipo === 1) {
      console.log(error);
      this.error = error;
      this.cargando = false;
      this.detalles = error.error.error
      this.msgs1 = [{ severity: 'error', summary: 'Error!', detail: 'Error inesperado, favor de contactar a soporte.' }];
    } else if (tipo === 2) {
      console.log(error);
      if (elem != null) {
        elem.ejecutado = 2;
        elem.error = error.error.message;
      }
      this.error = true;
      this.cargando = false;
      this.detalles = error.error.error;
      if (this.log_ambiente.log_id != undefined) {
        this.actualizarEstadoLog(2, this.detalles);
      }
      this.msgs1 = [{ severity: 'error', summary: 'Error!', detail: 'Ocurrió un error en ' + nombre + ', por favor verificar y reintentar.' }];
    }
  }

  //funcion para mostrar los detalles de un error tipo 2
  detallesMsg() {
    this.detail=[{ severity: 'info', summary: 'Error!', detail: this.detalles }];
    this.detalles=undefined;
  }


}
