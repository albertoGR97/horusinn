import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LectoresRoutingModule } from './lectores-routing.module';
import{ LectoresComponent } from './lectores.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NavigationModule} from '../../shared/navigation/navigation.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {IconsModule} from '../../../modules/icons/icons.module';
import {CarouselModule} from 'primeng/carousel';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {LoadingModule} from '../../shared/loading/loading.module';
import {InputTextModule} from 'primeng/inputtext';
import {MultiSelectModule} from 'primeng/multiselect';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { MessageModule } from 'primeng/message';



@NgModule({
  declarations: [LectoresComponent],
    imports: [
        DialogModule,
        CommonModule,
        LectoresRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MessageModule,
        FontAwesomeModule,
        NavigationModule,
        IconsModule,
        CarouselModule,
        TableModule,
        ButtonModule,
        LoadingModule,
        InputTextModule,
        MultiSelectModule,
        DropdownModule
    ]
})

export class LectoresModule { }
