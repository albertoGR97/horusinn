import { Component, HostListener, OnInit } from '@angular/core';
import { PrimeNGConfig, MessageService, Message } from 'primeng/api';
import { ClientesModel, BitacoraLector } from '../../../models/lector/lector.model'
import { LectoresService } from '../../../services/lectores.service';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { interval, of, Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import { count } from 'console';

@Component({
  selector: 'app-lectores',
  templateUrl: './lectores.component.html',
  styleUrls: ['./lectores.component.css'],
  providers: [MessageService]
})
export class LectoresComponent implements OnInit {


  @HostListener('document:mousemove', ['$event'])
  onMouseMove(e) {
    if (this.cargando == false && this.activo == false) {
      this.scrollAlto();
        this.repetir = setTimeout(() => {
          this.scrollRepetir();
        }, 3000);
        sessionStorage.setItem('repetir',this.repetir);
    }
  }

  private clientesG: ClientesModel[] = [];
  private clientesMA: ClientesModel[] = [];
  private clientesCC: ClientesModel[] = [];
  private clientesZK: ClientesModel[] = [];
  clientesGmostrar: ClientesModel[] = [];
  clientesMAmostrar: ClientesModel[] = [];
  clientesCCmostrar: ClientesModel[] = [];
  clientesZKmostrar: ClientesModel[] = [];
  respu: ClientesModel[] = [];
  estatusLector: ClientesModel[] = [];
  cargando = false;
  activo = false;
  numMostrar = 8;
  numRecorrer = 8;
  scrollController = true;
  seguimientoRegistro = false;
  seguimientoBitacora = false;
  infoLector = false;
  estatusReporte: any[];
  columsBitacora: any[];
  bitacoraLector: any[];
  private cambios = [];
  totalLinea = 0;
  totalLineaM = 0;
  totalDesconectadoM = 0;
  totalDesconectado = 0;
  totalLectores = 0;
  onOffBitacoraGeneral = false;
  idBitacoraGeneral = 0;
  nombreBitacoraGeneral = '';
  actualizaButton = false;
  loading = false;
  bitacoranewLector = new BitacoraLector;
  bitacoraForm: FormGroup;
  buscar = "";
  tiempo = 4000;
  total;
  estatusFilter;
  estatusSelected=[
    { name: "Fuera de línea con marcajes", code: "lector_no_activo_marcaje" },
    { name: "Fuera de línea", code: "lector_no_activo" }
  ];

  //scroll v2.
  repetir;
  suscripcion: Subscription;
  ultimaActualizacion ="";


  constructor(private fb: FormBuilder, private messageService: MessageService, private lectoresService: LectoresService, private router: Router, private primengConfig: PrimeNGConfig) {
    this.getClientes();
    this.estatusReporte = [
      { label: 'Atendido', value: 'atendido' },
      { label: 'Pendiente', value: 'pandiente' }
    ];

    this.columsBitacora = [
      { field: 'estatus', header: 'Estatus'},
      { field: 'problema', header: 'Problema'},
      { field: 'observacion', header: 'Observación'},
      { field: 'comentario_cliente', header: 'Comentario del cliente'},
      { field: 'created_at', header: 'Fecha registro'},
      { field: 'registro', header: 'Usuario registro'},
    ];
  }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.estatusFilter = [
      { name: "En línea con marcajes", code: "lector_conectado" },
      { name: "En línea sin marcajes", code: "lector_activo_no_marcaje" },
      { name: "Fuera de línea con marcajes", code: "lector_no_activo_marcaje" },
      { name: "Fuera de línea", code: "lector_no_activo" },
    ];

    this.bitacoraForm = this.fb.group({
      'estatus': new FormControl('', Validators.required),
      'problema': new FormControl('', Validators.required),
      'observacion': new FormControl('', Validators.required),
      'comentario_cliente': new FormControl('', Validators.required),
    });
  }

  async getClientes(recarga = true) {
    $('html, body').scrollTop(0);
    this.cargando = true;
    await new Promise((resolve, reject) => {
      this.lectoresService.getClientes().subscribe(
        resp => {
          this.respu = resp;
          this.total = resp.length;
          if(resp.length > 0){
            this.ultimaActualizacion = resp[0]['ultima_actualizacion'];
          }else{
            this.ultimaActualizacion = "No hay datos disponibles"
          }
          this.cargando = false;
          this.clientesG = resp.filter(elem => elem.producto == 'girha');
          this.clientesMA = resp.filter(elem => elem.producto == 'mi_asistencia');
          this.clientesCC = resp.filter(elem => elem.producto == 'cloudclock');
          this.clientesZK = resp.filter(elem => elem.producto == 'zk');

          resolve();
        },
        error => {
          console.log(error);
        });
    });

    /*for (const elem of this.clientesG) {
      await new Promise((resolve, reject) => {
        this.lectoresService.getLectoresByCliente(elem).subscribe(
          resp => {
            elem.lectores = resp;
            resolve();
          }
        );
      });
    }
    console.log(this.clientesG);*/
    this.clientesG.sort(function (a, b) {
      if (a.lectores.length < b.lectores.length) {
        return 1;
      }
    });
    /*for (const elem of this.clientesMA) {
      await new Promise((resolve, reject) => {
        this.lectoresService.getLectoresByCliente(elem).subscribe(
          resp => {
            elem.lectores = resp;
            resolve();
          }
        );
      });
    }*/
    this.clientesMA.sort(function (a, b) {
      if (a.lectores.length < b.lectores.length) {
        return 1;
      }
    });
    /*for (const elem of this.clientesCC) {
      await new Promise((resolve, reject) => {
        this.lectoresService.getLectoresByCliente(elem).subscribe(
          resp => {
            elem.lectores = resp;
            resolve();
          }
        );
      });
    }*/
    this.clientesCC.sort(function (a, b) {
      if (a.lectores.length < b.lectores.length) {
        return 1;
      }
    });
    /*for (const elem of this.clientesZK) {
      await new Promise((resolve, reject) => {
        this.lectoresService.getLectoresByCliente(elem).subscribe(
          resp => {
            elem.lectores = resp;
            resolve();
          }
        );
      });
    }*/
    this.clientesZK.sort(function (a, b) {
      if (a.lectores.length < b.lectores.length) {
        return 1;
      }
    });

    this.clientesGmostrar = JSON.parse(JSON.stringify(this.clientesG));
    this.clientesMAmostrar = JSON.parse(JSON.stringify(this.clientesMA));
    this.clientesCCmostrar = JSON.parse(JSON.stringify(this.clientesCC));
    this.clientesZKmostrar = JSON.parse(JSON.stringify(this.clientesZK));
    this.cargando = false;

    //this.recargar();
    if(recarga){
      this.scrollRepetir();
    }
   this.buscarDatos(false);
  }

  ngOnDestroy(){
    this.scrollAlto();
  }

  scrollRepetir(){
    if(this.scrollController){
      const repetir = interval(30);
      this.suscripcion = repetir.subscribe(val=>this.scrollAbajo());
    }
  }

  scrollStop(){
    if(this.scrollController){
      this.scrollController = false;
    }else{
      this.scrollController = true;
    }
  }

  scrollAbajo() {
    window.scrollBy(0, 1);
    if (window.innerHeight + window.scrollY == $(document).height()) {
      // console.log("arriba");
      $('html, body').animate({ scrollTop: 0 }, 100);
    }
  }

  scrollAlto() {
    if(this.ultimaActualizacion != "No hay datos disponibles"){
      this.suscripcion.unsubscribe();
      clearTimeout(this.repetir);
    }
  }

  buscarDatos(recarga = true) {
    if (recarga){
      this.getClientes(false);
      this.scrollAlto();
    }
    this.clientesGmostrar = JSON.parse(JSON.stringify(this.clientesG));
    this.clientesMAmostrar = JSON.parse(JSON.stringify(this.clientesMA));
    this.clientesCCmostrar = JSON.parse(JSON.stringify(this.clientesCC));
    this.clientesZKmostrar = JSON.parse(JSON.stringify(this.clientesZK));
    if (this.buscar.length > 0 || this.estatusSelected.length > 0) {

      this.clientesGmostrar = this.clientesGmostrar.filter(elem => {
        if (this.buscar.length > 0){
          if (elem.nombre.toUpperCase().includes(this.buscar.toUpperCase()) || elem.nombre_db.toUpperCase().includes(this.buscar.toUpperCase())) {
            if (this.estatusSelected.length > 0){
              elem.lectores = elem.lectores.filter(lector => {
                return this.validaEstatusBusque(lector);
              });
            }
            return elem;
          }
        } else {
          if (this.buscar.length > 0){
            elem.lectores = elem.lectores.filter(lector => {
              return lector.no_serie.toUpperCase().includes(this.buscar.toUpperCase());
            });
          }
          if (this.estatusSelected.length > 0){
            elem.lectores = elem.lectores.filter(lector => {
              return this.validaEstatusBusque(lector);
            });
            if (!(elem.lectores.length > 0) && this.estatusSelected.length!=4){
              return false;
            }
          }
          return elem;
        }
      });
      this.clientesMAmostrar = this.clientesMAmostrar.filter(elem => {
        if (this.buscar.length > 0){
          if (elem.nombre.toUpperCase().includes(this.buscar.toUpperCase()) || elem.nombre_db.toUpperCase().includes(this.buscar.toUpperCase())) {
            if (this.estatusSelected.length > 0){
              elem.lectores = elem.lectores.filter(lector => {
                return this.validaEstatusBusque(lector);
              });
            }
            return elem;
          }
        }
        else {
          if (this.buscar.length > 0){

            elem.lectores = elem.lectores.filter(lector => {
              return lector.no_serie.toUpperCase().includes(this.buscar.toUpperCase());
            });
          }
          if (this.estatusSelected.length > 0){
            elem.lectores = elem.lectores.filter(lector => {
              return this.validaEstatusBusque(lector);
            });
            if (!(elem.lectores.length > 0) && this.estatusSelected.length!=4){
              return false;
            }
          }
          return elem;
        }
      });
      this.clientesCCmostrar = this.clientesCCmostrar.filter(elem => {
        if (this.buscar.length > 0){
          if (elem.nombre.toUpperCase().includes(this.buscar.toUpperCase()) || elem.nombre_db.toUpperCase().includes(this.buscar.toUpperCase())) {
            if (this.estatusSelected.length > 0){
              elem.lectores = elem.lectores.filter(lector => {
                return this.validaEstatusBusque(lector);
                console.log(this.validaEstatusBusque(lector));
              });
            }
            return elem;
          }
        }
        else {
          if (this.buscar.length > 0){

            elem.lectores = elem.lectores.filter(lector => {
              return lector.no_serie.toUpperCase().includes(this.buscar.toUpperCase());
            });
          }
          if (this.estatusSelected.length > 0){
            elem.lectores = elem.lectores.filter(lector => {
              return this.validaEstatusBusque(lector);
            });
            if (!(elem.lectores.length > 0) && this.estatusSelected.length!=4){
              return false;
            }
          }
          return elem;
        }
      });
      this.clientesZKmostrar = this.clientesZKmostrar.filter(elem => {
        if (this.buscar.length > 0){
          if (elem.nombre.toUpperCase().includes(this.buscar.toUpperCase()) || elem.nombre_db.toUpperCase().includes(this.buscar.toUpperCase())) {
            if (this.estatusSelected.length > 0){
              elem.lectores = elem.lectores.filter(lector => {
                return this.validaEstatusBusque(lector);
              });
            }
            return elem;
          }
        }
        else {
          if (this.buscar.length > 0){

            elem.lectores = elem.lectores.filter(lector => {
              return lector.no_serie.toUpperCase().includes(this.buscar.toUpperCase());
            });
          }
          if (this.estatusSelected.length > 0){
            elem.lectores = elem.lectores.filter(lector => {
              return this.validaEstatusBusque(lector);
            });
            if (!(elem.lectores.length > 0) && this.estatusSelected.length!=4){
              return false;
            }
          }
          return elem;
        }
      });

    }

    this.total = this.clientesGmostrar.length + this.clientesMAmostrar.length + this.clientesCCmostrar.length + this.clientesZKmostrar.length;
  }

  validaEstatusBusque(lector){
    let statusBucar = JSON.parse(JSON.stringify(this.estatusSelected));
    let lector_conectado = (statusBucar.filter(elem => elem.code == 'lector_conectado'))[0];
    let lector_activo_no_marcaje = (statusBucar.filter(elem => elem.code == 'lector_activo_no_marcaje'))[0];
    let lector_no_activo_marcaje = (statusBucar.filter(elem => elem.code == 'lector_no_activo_marcaje'))[0];
    let lector_no_activo = (statusBucar.filter(elem => elem.code == 'lector_no_activo'))[0];

    if (lector.estatus == 1){
      if (lector.marcajes_hoy > 0 && lector_conectado != undefined){
        console.log("lector_conectado");
          return true;
      }else if (!(lector.marcajes_hoy > 0) && lector_activo_no_marcaje != undefined){
        return true;
      }
    }else {
      if (lector.marcajes_hoy > 0 && lector_no_activo_marcaje != undefined){
        console.log("lector_no_activo_marcaje");
        return true;
      }else if( !(lector.marcajes_hoy > 0) && lector_no_activo != undefined){
        return true;
      }
    }

    return false;
  }

  recargar() {
    setTimeout(() => {
      this.scrollAlto();
      window.location.reload();
    }, 300000);
  }

  generaExcel(clientesGmostrar, clientesMAmostrar, clientesCCmostrar, clientesZKmostrar){
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet("Lectores"); // Nombre de la hoja
    const fname="Lectores"; // Nombre del archivo

    worksheet.columns = [
      { header: 'Sistema', key: 'sistema', width: 15},
      { header: 'Cliente', key: 'cliente', width: 20},
      { header: 'Base de datos', key: 'db', width: 15},
      { header: 'Nombre equipo', key: 'equipo', width: 20},
      { header: 'Ultima conexión', key: 'conexionU', width: 20},
      { header: 'Estatus', key: 'estatus', width: 12},
      { header: 'Usuarios cargados', key: 'usuariosC', width: 17},
      { header: 'Ultimo marcaje', key: 'marcajeU', width: 20},
    ];

    if(clientesGmostrar != ''){
      CrearRow(clientesGmostrar);
    }
    if(clientesMAmostrar != ''){
      CrearRow(clientesMAmostrar);
    }
    if(clientesCCmostrar != ''){
      CrearRow(clientesCCmostrar);
    }
    if(clientesZKmostrar != ''){
      CrearRow(clientesZKmostrar);
    }

    function CrearRow(respu){
      for (let res of respu){
        for(let lectores of res.lectores){
          let producto = "";
          switch (res.producto){
            case 'girha': producto = 'Girha'; break;
            case 'mi_asistencia': producto = 'Mi Asistencia'; break;
            case 'cloudclock': producto = 'CloudClock'; break;
            default: producto = 'ZK'; break;
          };
          worksheet.addRow({
            sistema: producto, cliente: res.nombre, db: res.nombre_db.substr(8,11), equipo: lectores.nombre, conexionU: lectores.ultima_conexion,
            estatus: (lectores.estatus === 1 ? 'En línea' : 'Desconectado'), usuariosC: lectores.marcajes_hoy, marcajeU: res.ultima_actualizacion
          });
        }
      }
    }

    workbook.xlsx.writeBuffer().then((data) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, fname+'-'+new Date().valueOf()+'.xlsx');
    });
  }

  generaExcelBitacora(dataLector, nombreLector, serieLector){
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet("Bitacora"); // Nombre de la hoja
    const fname="Bitacora lector "+nombreLector+" - "+serieLector; // Nombre del archivo

    worksheet.columns = [
      { header: 'Registro', key: 'FRegistro', width: 20},
      { header: 'Usuario registro', key: 'URegisto', width: 17},
      { header: 'Estatus', key: 'Estatus', width: 10},
      { header: 'Problema', key: 'Problema', width: 30},
      { header: 'Observación', key: 'Observacion', width: 30},
      { header: 'Comentario cliente', key: 'CCliente', width: 17},
      { header: 'Actualización', key: 'FUpdate', width: 20},
      { header: 'Usuario actualizo', key: 'UUpdate', width: 17},
    ];

    if(dataLector != ''){
      CrearRow(dataLector);
    }

    function CrearRow(respu){
      for (let res of respu){
        worksheet.addRow({
          FRegistro: res.created_at, URegisto: res.registro_nombre, Estatus: res.estatus, Problema: res.problema, Observacion: res.observacion, CCliente: res.comentario_cliente, FUpdate: res.updated_at, UUpdate: res.actualizo_nombre
        });
      }
    }

    workbook.xlsx.writeBuffer().then((data) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, fname+'-'+new Date().valueOf()+'.xlsx');
    });
  }

  actualizaInfoLectores() {
    this.cargando = true;
    this.lectoresService.getAllLectores().subscribe(resp => {
      this.cargando = false;
      window.location.reload();
    });
  }

  registroLectore(lectorSelect, op = 1){
    this.cargando = true;
    this.seguimientoRegistro = false;
    this.cambios = [];
    this.actualizaButton = true;
    this.scrollController = false;

    if(op == 1){
      this.infoLector = lectorSelect
      this.lectoresService.getLectoresByCliente(this.infoLector).subscribe(resp => {
        this.bitacoraLector = resp;
        this.cargando = false;
      }, error => {
        console.log(error);
      });
    }else if(op == 2){
      this.idBitacoraGeneral = lectorSelect.id;
      this.nombreBitacoraGeneral = lectorSelect.nombre;
      this.onOffBitacoraGeneral = true;
      this.cargando = false;

      lectorSelect['op'] = 2;
      lectorSelect['ip_psql'] = null;

      this.lectoresService.getLectoresByCliente(lectorSelect).subscribe(resp => {
        this.bitacoraLector = resp;
        this.cargando = false;
      }, error => {
        console.log(error);
      });
    }

    this.seguimientoRegistro = true;
  }

  closeRegistroLectore(){
    this.seguimientoRegistro = false;
    this.cambios = [];
    this.actualizaButton = true;
    this.onOffBitacoraGeneral = false;
  }

  agregarRegistroBitacora(bitacoranewLector: BitacoraLector, lector){
    this.bitacoranewLector['registra'] = localStorage.getItem('user_id_auth');
    this.bitacoranewLector['no_serie'] = lector['no_serie'];
    this.bitacoranewLector['lector'] = lector['nombre'];

    this.lectoresService.savebitacoraByCliente(this.bitacoranewLector).subscribe(resp => {
      this.seguimientoBitacora = false;
      this.bitacoranewLector = new BitacoraLector;
      this.registroLectore(lector);
    }, error => {
      console.log(error);
    });
  }

  closeAgregarRegistroBitacora(lectorSelect){
    this.seguimientoBitacora = false;
    this.bitacoranewLector = new BitacoraLector;

    console.log('*******************************');
    console.log(lectorSelect  );
    console.log('*******************************');

    if(this.onOffBitacoraGeneral){
      this.registroLectore(lectorSelect, 2);
    }else{
      this.registroLectore(lectorSelect);
    }
  }

  actualizaLectore(){
    this.seguimientoRegistro = false;
    this.seguimientoBitacora = true;
  }

  //valida si ya hubo un cambio antes en el mismo registro
  private validaUpdateArray($id) {
    var validador = this.cambios.findIndex((elem) => elem.id === $id);

    return validador;
  }

  actualizaCampo($campo, $id, $valor, fila = null) {
    if (($valor != null || $valor != '') && $valor.length > 3) {

      this.actualizaButton = false;
    } else {
      this.actualizaButton = true;
    }
    var validador = this.validaUpdateArray($id);
    if (validador != -1) {
      this.cambios[validador][$campo] = $valor;
    } else {
      fila.actualizo = localStorage.getItem('user_id_auth');
      console.log(fila);
      this.cambios.push(fila);
    }
  }

  //actualizar
  toUpdateServers() {
    this.loading = true;
    this.lectoresService.updateBitacora(this.cambios).subscribe(
      resp => {
        this.loading = false;
        this.actualizaButton = true;
        this.messageService.clear();
        this.seguimientoRegistro = false;
        this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: resp['message'] });
      }, error => {
        this.loading = false;
        this.actualizaButton = true;
        this.messageService.add({ key: 'tc', severity: 'error', summary: 'Error', detail: error });
      }
    );
    this.cambios = [];
  }
}