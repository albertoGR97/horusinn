import { Component, OnInit } from '@angular/core';
import {UsuarioModel} from '../../../models/usuario/usuario.model';
import {MessageService, PrimeNGConfig} from 'primeng/api';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-reset-key',
  templateUrl: './reset-key.component.html',
  styleUrls: ['./reset-key.component.css'],
  providers: [MessageService]
})
export class ResetKeyComponent implements OnInit {

  usuario: UsuarioModel;
  cargando = false;


  constructor(private messageService: MessageService, private authService: AuthService,
              private router: Router, private httpClient: HttpClient,
              private primengConfig: PrimeNGConfig) { }

  ngOnInit(): void {
    this.usuario = new UsuarioModel();
    this.primengConfig.ripple = true;
  }


  resetKey(form: NgForm): void{
    if (!form.invalid ){

      this.messageService.clear();
      this.messageService.add({key: 'c', sticky: true, severity: 'warn', summary: '¿Está seguro de generar una nueva su llave?', detail: 'La llave actual quedara inservible y se enviara correo para iniciar el proceso de solicitud de una nueva'});

    }else{
      this.messageService.clear();
      this.messageService.add({key: 'tc', severity: 'warn', summary: 'Campos', detail: 'Debe llenar los campos obligatorios'});
    }
  }

  validateEmail(email): boolean {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  onReject() {
    this.messageService.clear('c');
  }

  onConfirm(){
    this.messageService.clear();
    this.cargando = true;
    this.authService.resetKeyRequest(this.usuario)
      .subscribe( resp => {
        console.log(resp);
        this.cargando = false;
        this.messageService.clear();
        this.messageService.add({key: 'tc', severity: 'success', summary: 'Solicitud enviada', detail: 'Favor de revisar su correo electrónico para continuar con el proceso.'});
      }, (err) => {
        this.cargando = false;
        console.log(err);
        this.messageService.clear();

        err['error']['message'] == undefined ?
        this.messageService.add({key: 'tc', severity: 'error', summary: 'Error', detail: 'Error al realizar la solicitud, por favor inténtelo más tarde.'+' (g-500)'})
        : err['error']['message'] == 'Su solicitud esta en proceso' ? this.messageService.add({key: 'tc', severity: 'warn', summary: 'En proceso', detail: err['error']['message']+' ('+err['error']['type']+')'}) : this.messageService.add({key: 'tc', severity: 'error', summary: 'Error', detail: err['error']['message']+' ('+err['error']['type']+')'}) ;
      } );

  }

  backLogin(){
    this.router.navigateByUrl('/login');

  }

}
