import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetKeyComponent } from './reset-key.component';

describe('ResetKeyComponent', () => {
  let component: ResetKeyComponent;
  let fixture: ComponentFixture<ResetKeyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetKeyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
