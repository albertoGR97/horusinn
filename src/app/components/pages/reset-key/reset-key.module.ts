import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResetKeyRoutingModule } from './reset-key-routing.module';
import { ResetKeyComponent } from './reset-key.component';
import {InputTextModule} from 'primeng/inputtext';
import {FormsModule} from '@angular/forms';
import {PasswordModule} from 'primeng/password';
import {FileUploadModule} from 'primeng/fileupload';
import {RippleModule} from 'primeng/ripple';
import {CheckboxModule} from 'primeng/checkbox';
import {LoadingModule} from '../../shared/loading/loading.module';
import {ToastModule} from 'primeng/toast';


@NgModule({
  declarations: [ResetKeyComponent],
  imports: [
    CommonModule,
    ResetKeyRoutingModule,
    InputTextModule,
    FormsModule,
    PasswordModule,
    FileUploadModule,
    RippleModule,
    CheckboxModule,
    LoadingModule,
    ToastModule
  ]
})
export class ResetKeyModule { }
