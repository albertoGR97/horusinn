import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResetKeyComponent } from './reset-key.component';

const routes: Routes = [{ path: '', component: ResetKeyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResetKeyRoutingModule { }
