import { Component, HostListener, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { AmbienteModel } from '../../../../../models/ambiente/ambiente.model';
import { AmbientesService } from '../../../../../services/ambientes.service';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { ServidorService } from '../../../../../services/servidor.service';
import { AlertaModel } from '../../../../../models/alerta/alerta.model';
import { AlertasService } from '../../../../../services/alertas.service';

@Component({
  selector: 'app-girha-alertas',
  templateUrl: './girha-alertas.component.html',
  styleUrls: ['./girha-alertas.component.css'],
  providers: [MessageService]
})
export class GirhaAlertasComponent implements OnInit {

  alertaList: AlertaModel[];
  alerta: AlertaModel;
  insertAlerta: AlertaModel[];
  brands: any[];

  cambios = [];
  collection = [];
  cont = 0;
  search = "";
  cols: any[];
  colsInsert: any[];
  selectedColumns: any[];
  selectedAlerta: any;
  formulario: true;
  arg = 6;
  display = true;
  loading = false;
  errorGral = false;
  mostrar = false;
  msj = '';

  submitted: boolean;
  alertaForm: FormGroup;
  alertaInsertform: FormGroup;

  // Validacion y bloqueos
  updateButtonStatus: boolean = true;
  deleteButtonStatus: boolean = true;
  insertButtonStatus: boolean = true;
  deleteInsertButtonStatus: boolean = true;
  cerrarInsertButtonStatus: boolean = false;
  validacionIDCLAVE: boolean = false;


  userCanRead: any;
  userCanEdit: any;
  userCanDelete: any;
  userCanLoad: any;
  userCanInsert: any;

  fileToUpload: any;
  currentRoute: any;
  userPermissions: any[];

  constructor(private fb: FormBuilder, private messageService: MessageService, private router: Router,
    private servidorService: ServidorService, private alertasService: AlertasService) {
    this.loading = true;
    this.currentRoute = this.router.url;
    this.alertasService.getAllAlerts()
      .subscribe(resp => {
        this.loading = false;
        this.alertaList = resp;
        if (this.alertaList.length > 0) {
          this.mostrar = true;
        }
      });

    this.brands = [
      { label: 'Activo', value: 1 },
      { label: 'Desactivado', value: 0 }];

  }

  ngOnInit(): void {

    this.userCanRead = 1;
    this.userCanEdit = 1;
    this.userCanDelete = 0;
    this.userCanLoad = 1;
    this.userCanInsert = 1;

    this.alertaForm = this.fb.group({
      'nombre': new FormControl('', Validators.required),
      'estado': new FormControl('', Validators.required),
      'hora': new FormControl('', Validators.required),
      'correos': new FormControl('', Validators.required),
    });

    this.cols = [
      { field: 'nombre', header: 'Nombre', width: '150px' },
      { field: 'estado', header: 'Estado', width: '150px' },
      { field: 'hora', header: 'Hora', width: '150px' },
      { field: 'correos', header: 'Correo de notificación', width: '150px' },
    ];

    this.selectedColumns = this.cols;
    this.alertaList = [];
  }

  buscarDatos() { }

  callFormInsert() {
    this.alertaInsertform = this.fb.group({
      'nombre': new FormControl('', Validators.required),
      'estado': new FormControl('', Validators.required),
      'hora': new FormControl('', Validators.required),
      'correos': new FormControl('', Validators.required),
    });

    this.colsInsert = [
      { field: 'id', header: 'Id', width: '50px' },
      { field: 'nombre', header: 'Nombre', width: '150px' },
      { field: 'estado', header: 'Estado', width: '150px' },
      { field: 'hora', header: 'Hora', width: '150px' },
      { field: 'correos', header: 'Correo de notificación', width: '250px' },
    ];

    this.insertAlerta = [
      { id: 1, nombre: '', estado: 2, hora: '', correos: '' },
      { id: 2, nombre: '', estado: 2, hora: '', correos: '' },
      { id: 3, nombre: '', estado: 2, hora: '', correos: '' },
      { id: 4, nombre: '', estado: 2, hora: '', correos: '' },
      { id: 5, nombre: '', estado: 2, hora: '', correos: '' },
    ];

    this.display = true;
    this.formulario = true;

  }

  toBorrar() {
    this.loading = true;
    let datos = [];

    for (let i = 0; i < this.selectedAlerta.length; i++) {
      datos.push(this.selectedAlerta[i]['nombre']);
    }

    this.servidorService.deletegeneral(datos).subscribe(
      resp => {

        this.loading = false;
        this.servidorService.getAllServers()
          .subscribe(servers => {
            this.loading = false;
            this.alertaList = servers;

            this.messageService.clear();
            this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: 'Servidor eliminado correctamente' });
          });
      });
  }


  toUpdate() {

    this.alertasService.validarServer().subscribe(
      resp => {
        this.alertasService.update(this.cambios).subscribe(
          resp => {
            this.alertasService.getAllAlerts()
              .subscribe(servers => {
                this.loading = false;
                this.alertaList = servers;
                this.updateButtonStatus = true;
                this.messageService.clear();
                this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: 'Alerta(s) actualizada(s) correctamente' });
              });
          },
        );
        this.cambios = [];
      },
      error => this.errorHandler(error, 1));
  }

  makeButtonVisible(event) {
    let total = this.alertaList.length;
    let posicion = event.index;
    let activar = this.selectedAlerta.length;
    if (activar === 1) {
      this.deleteButtonStatus = true;
      for (let i = 0; i < total; i++) {
        if (posicion === i) {
          this.alertaList[posicion]['activo'] = 0;
        } else {
          this.alertaList[i]['activo'] = 1;
        }
      }
    } else if (activar < 1) {
      this.deleteButtonStatus = false;
    } else {
      this.deleteButtonStatus = false;
      for (let i = 0; i < total; i++) {
        this.alertaList[i]['activo'] = 1;
      }
    }
  }

  activeUpdateButton(data) {
    if (data != null || data != '') {

      this.updateButtonStatus = false;
    } else {
      this.updateButtonStatus = true;
    }
  }

  onBlurMethod($campo, $id, $valor, fila = null) {

    this.activeUpdateButton($valor)
    var validador = this.validaUpdateArray($id);
    if (validador != -1) {
      this.cambios[validador][$campo] = $valor;
    } else {
      this.cambios.push(fila);
    }
  }


  deleteIndividualRow(id) {
    this.loading = true;
    this.servidorService.deleteServer(id).subscribe(resp => {
      this.servidorService.getAllServers()
        .subscribe(servers => {
          this.loading = false;
          this.alertaList = servers;

          this.messageService.clear();
          this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: 'Servidor eliminado correctamente' });
        });
    });
  }

  close() {

    setTimeout(() => {
      this.alertaInsertform.reset();
      this.alertaForm.reset();
    }, 0);
    this.errorGral = false;
    this.display = false;

  }

  addRow() {
    this.insertAlerta.push({ id: this.arg, nombre: '', estado: 2, hora: '', correos: '' },);
    this.arg++;
  }

  store() {
    this.limpiaInsertArray();
    let datos = this.insertAlerta;

    this.alertasService.validarServer().subscribe(
      resp => {
        this.alertasService.save(datos).subscribe(resp => {
          this.insertAlerta = [
            { id: 1, nombre: '', estado: 2, hora: '', correos: '' },
            { id: 2, nombre: '', estado: 2, hora: '', correos: '' },
            { id: 3, nombre: '', estado: 2, hora: '', correos: '' },
            { id: 4, nombre: '', estado: 2, hora: '', correos: '' },
            { id: 5, nombre: '', estado: 2, hora: '', correos: '' },
          ];
          this.insertButtonStatus = true;
          this.alertasService.getAllAlerts()
            .subscribe(alerts => {
              this.loading = false;
              this.alertaList = alerts;
              this.display = false;
              this.messageService.clear();
              this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: 'Alerta(s) insertada(s) correctamente' });
            }, error => this.errorHandler(error, 1));
          this.errorGral = false;
        });
      },
      error => this.errorHandler(error, 1));
  }

  removeRow(id) {
    this.arg--;
    for (let i = 0; i < this.insertAlerta.length; i++) {
      if (this.insertAlerta[i]['id'] == id) {
        this.insertAlerta.splice(i, 1);
      }
    }

    for (let j = 0; j < this.insertAlerta.length; j++) {
      this.insertAlerta[j]['id'] = j + 1;
    }
  }

  limpiaInsertArray() {
    this.insertAlerta = this.insertAlerta.filter(elem => {
      return elem.nombre !== '' || elem.estado < 2 || elem.hora !== '' || elem.correos !== '';
    });
  }


  validaInsertArray() {
    let valida = this.insertAlerta.filter(elem => {
      return elem.nombre !== '' || elem.estado < 2 || elem.hora !== '' || elem.correos !== '';
    });
    valida = valida.filter(elem => {
      return elem.nombre.length < 3 || elem.estado == 2 || elem.hora === '' || elem.correos === '';
    });

    this.insertButtonStatus = (valida.length > 0);

  }

  validaNombre(nombre) {
    let repetidoInsert = (this.insertAlerta.filter(elem => elem.nombre === nombre && elem.nombre !== '')).length > 1;
    if ((repetidoInsert) || ((this.alertaList.filter(elem => elem.nombre === nombre)).length > 0)) {
      this.msj = 'Nombre repetido';
      return true;
    } else {
      this.msj = '';
      return false;
    }
  }

  private validaUpdateArray($id) {
    var validador = this.cambios.findIndex((elem) => elem.id === $id);
    return validador;
  }

  errorHandler(error, tipo, elem = null) {
    if (tipo === 1) {
      console.log(error);
      this.messageService.clear();
      this.messageService.add({ key: 'tc', severity: 'error', summary: 'Error', detail: error['error']['message'] });
    } else if (tipo === 2) {
      console.log(error);
      elem.ejecutado = 2;
      elem.error = error.error.message;
      this.messageService.clear();
      this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: 'Datos insertados correctamente' });
    }

  }

}
