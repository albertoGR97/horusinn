import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GirhaAlertasComponent } from './girha-alertas.component';

describe('GirhaAlertasComponent', () => {
  let component: GirhaAlertasComponent;
  let fixture: ComponentFixture<GirhaAlertasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GirhaAlertasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GirhaAlertasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
