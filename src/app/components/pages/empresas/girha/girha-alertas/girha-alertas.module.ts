import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GirhaAlertasRoutingModule } from './girha-alertas-routing.module';
import { GirhaAlertasComponent } from './girha-alertas.component';
import {NavigationModule} from '../../../../shared/navigation/navigation.module';
import {MultiSelectModule} from 'primeng/multiselect';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ButtonModule} from 'primeng/button';
import {MessageModule} from 'primeng/message';
import {TableModule} from 'primeng/table';
import {InputTextModule} from 'primeng/inputtext';
import {TooltipModule} from 'primeng/tooltip';
import {DropdownModule} from 'primeng/dropdown';
import {CheckboxModule} from 'primeng/checkbox';
import {DialogModule} from 'primeng/dialog';
import {ToastModule} from 'primeng/toast';


@NgModule({
  declarations: [GirhaAlertasComponent],
  imports: [
    CommonModule,
    GirhaAlertasRoutingModule,
    NavigationModule,
    MultiSelectModule,
    FormsModule,
    ButtonModule,
    ReactiveFormsModule,
    MessageModule,
    TableModule,
    InputTextModule,
    TooltipModule,
    DropdownModule,
    CheckboxModule,
    DialogModule,
    ToastModule
  ]
})
export class GirhaAlertasModule { }
