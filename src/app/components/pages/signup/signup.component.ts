import { Component, OnInit } from '@angular/core';
import {UsuarioModel} from '../../../models/usuario/usuario.model';
import {MessageService, PrimeNGConfig} from 'primeng/api';
import {AuthService} from '../../../services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [MessageService]
})
export class SignupComponent implements OnInit {

  usuario: UsuarioModel;
  cargando = false;
  dirty = false;


  constructor(private messageService: MessageService, private authService: AuthService,
              private router: Router,
              private primengConfig: PrimeNGConfig, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.usuario = new UsuarioModel();
    this.primengConfig.ripple = true;
  }


  enviar(form: NgForm): void{
    if (!form.invalid && form.controls.email.touched && form.controls.email.dirty){
      this.cargando = true;
      this.authService.signup(this.usuario)
        .subscribe( resp => {
          this.cargando = false;
          sessionStorage.setItem('creado','1');
          this.router.navigate(['login']);
        }, err => {
          this.cargando = false;
          this.messageService.clear();
          this.messageService.add({key: 'tc', severity: 'error', summary: 'Datos erroneos', detail: err['error']['message']+' ('+err['error']['type']+')'});
        } );
    }else{
      if (form.controls.email.touched || form.controls.email.dirty){
        this.messageService.clear();
        this.messageService.add({key: 'tc', severity: 'error', summary: 'Campos', detail: 'El correo fue modificado, no es posible realizar ninguna acción.'});
      }else{
        this.messageService.clear();
        this.messageService.add({key: 'tc', severity: 'error', summary: 'Campos', detail: 'Debe llenar los campos obligatorios.'});
      }

    }
  }
}
