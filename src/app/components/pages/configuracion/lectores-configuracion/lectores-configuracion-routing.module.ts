import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LectoresConfiguracionComponent } from './lectores-configuracion.component';

const routes: Routes = [{ path: '', component: LectoresConfiguracionComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LectoresConfiguracionRoutingModule { }
