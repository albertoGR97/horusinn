import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LectoresConfiguracionRoutingModule } from './lectores-configuracion-routing.module';
import { LectoresConfiguracionComponent } from './lectores-configuracion.component';
import {NavigationModule} from '../../../shared/navigation/navigation.module';
import {MultiSelectModule} from 'primeng/multiselect';
import {MessageModule} from 'primeng/message';
import {TableModule} from 'primeng/table';
import {TooltipModule} from 'primeng/tooltip';
import {DropdownModule} from 'primeng/dropdown';
import {DialogModule} from 'primeng/dialog';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToastModule} from 'primeng/toast';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {RippleModule} from 'primeng/ripple';
import {CalendarModule} from 'primeng/calendar';


@NgModule({
  declarations: [LectoresConfiguracionComponent],
    imports: [
        CommonModule,
        LectoresConfiguracionRoutingModule,
        NavigationModule,
        MultiSelectModule,
        MessageModule,
        TableModule,
        TooltipModule,
        DropdownModule,
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        ToastModule,
        InputTextModule,
        ButtonModule,
        RippleModule,
        CalendarModule
    ]
})
export class LectoresConfiguracionModule { }
