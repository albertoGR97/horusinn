import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  text = 'Ups! La ruta a la que quieres acceder no existe.';

  constructor() { }

  ngOnInit(): void {
  }

}
