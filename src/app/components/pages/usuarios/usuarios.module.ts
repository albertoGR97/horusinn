import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosComponent } from './usuarios.component';
import {NavigationModule} from '../../shared/navigation/navigation.module';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {TableGeneralModule} from '../../shared/table/table-general.module';
import {TableModule} from 'primeng/table';
import {MultiSelectModule} from 'primeng/multiselect';
import {DropdownModule} from 'primeng/dropdown';
import {RippleModule} from 'primeng/ripple';
import {LoadingModule} from '../../shared/loading/loading.module';
import {PasswordModule} from 'primeng/password';
import {ToastModule} from 'primeng/toast';


@NgModule({
  declarations: [UsuariosComponent],
  imports: [
    CommonModule,
    UsuariosRoutingModule,
    NavigationModule,
    InputTextModule,
    ButtonModule,
    HttpClientModule,
    FormsModule,
    TableModule,
    MultiSelectModule,
    DropdownModule,
    RippleModule,
    LoadingModule,
    PasswordModule,
    ToastModule,
  ],
  providers: [

  ]
})
export class UsuariosModule { }
