import { TestBed } from '@angular/core/testing';

import { UsuariosPageService } from './usuarios-page.service';

describe('UsuariosPageService', () => {
  let service: UsuariosPageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsuariosPageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
