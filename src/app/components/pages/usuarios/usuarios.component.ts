import { Component, OnInit, ViewChild } from '@angular/core';
import { UsuarioModel } from '../../../models/usuario/usuario.model';
import { UsuariosPageService } from './usuarios-page.service';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';



@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css'],
  providers: [MessageService]
})

export class UsuariosComponent implements OnInit {

  tipos: any[];
  statuses: any[];
  loading = true;
  usuarios: UsuarioModel[];
  cols: any[];
  newUsuario: UsuarioModel;
  deleteUsuario: UsuarioModel;
  cargando = false;
  validUser = true;
  varEdit = false;
  @ViewChild('dt') table: Table;

  constructor(private usuariosPageService: UsuariosPageService, private authService: AuthService,
    private messageService: MessageService) { }

  ngOnInit(): void {
    this.newUsuario = new UsuarioModel();
    this.cols = [
      { field: 'nombre', header: 'Nombre' },
      { field: 'email', header: 'Correo' },
      { field: 'tipo', header: 'Tipo' },
      { field: 'status', header: 'Estatus' },

    ];
    this.tipos = [
      { label: 'Administrador', value: '1' },
      { label: 'Estandar', value: '2' },
    ];
    this.statuses = [
      { name: 'Pendiente', status: 0 },
      { name: 'Activo', status: 1 },
    ];


    this.usuariosPageService.getUsers().subscribe(resp => {
      this.usuarios = resp as UsuarioModel[];
      this.loading = false;
    });

  }

  editar(valor) {
    this.newUsuario = valor;
    this.varEdit = true;

  }

  nuevo(form: NgForm): void {
    this.cargando = true;
    if (this.varEdit) {
      if (!form.invalid) {
        this.usuariosPageService.editUser(this.newUsuario).subscribe(
          resp => {
            this.usuariosPageService.getUsers().subscribe(respU => {
              this.usuarios = respU as UsuarioModel[];
              this.reiniciaValores();
              this.messageService.clear();
              this.messageService.add({ key: 'tc', severity: 'success', summary: 'Modificación exitosa', detail: 'Se ha modificado exitosamente la información.' });
              this.varEdit = false;
            }, (err) => {
              this.cargando = false;
              console.log(err);
              this.messageService.clear();
              this.messageService.add({ key: 'tc', severity: 'error', summary: 'Error', detail: 'Error al consultar usuarios' });
            });
          }, (err) => {
            this.cargando = false;
            console.log(err);
            this.messageService.clear();
            this.messageService.add({ key: 'tc', severity: 'error', summary: 'Datos erroneos', detail: 'Favor de verificar los datos enviados' });
          });
      } else {
        this.cargando = false;
        this.messageService.clear();
        this.messageService.add({ key: 'tc', severity: 'error', summary: 'Campos', detail: 'Verificar los campos' });

      }

    } else {
      this.validUser = this.newUsuario.email !== undefined && !((this.usuarios.filter(user => user.email === this.newUsuario.email)).length > 0);

      if (!form.invalid && this.validUser) {
        this.authService.signup(this.newUsuario)
          .subscribe(resp => {
            this.usuariosPageService.getUsers().subscribe(respU => {
              this.usuarios = respU as UsuarioModel[];
              this.reiniciaValores();
              this.messageService.clear();
              this.messageService.add({ key: 'tc', severity: 'success', summary: 'Alta exitosa', detail: 'Se ha enviado un correo al área correspondiente para autorización del usuario.' });
            }, (err) => {
              this.cargando = false;
              console.log(err);
              this.messageService.clear();
              this.messageService.add({ key: 'tc', severity: 'error', summary: 'Error', detail: 'Error al consultar usuarios' });
            });
          }, (err) => {
            this.cargando = false;
            console.log(err);
            this.messageService.clear();
            this.messageService.add({ key: 'tc', severity: 'error', summary: 'Datos erroneos', detail: 'Favor de verificar los datos enviados' });
          });
      } else {
        this.cargando = false;
        this.messageService.clear();
        this.messageService.add({ key: 'tc', severity: 'error', summary: 'Campos', detail: 'Verificar los campos' });

      }
    }

  }

  borrar(data) {
    this.deleteUsuario = new UsuarioModel();
    this.deleteUsuario.id = data.id;
    this.deleteUsuario.email = data.email;
    this.messageService.clear();
    this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: '¿Está seguro de borrar a usuario ' + data.nombre + ' ?', detail: 'Esta acción no podrá ser revertida.' });

  }

  validateEmail(email): boolean {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  onStatusChange(event) {
    const info = [];
    event.value.forEach(elem => {
      info.push(elem.status);
    });
    this.table.filter(info, 'status', 'in');
  }
  onReject() {
    this.varEdit ? this.newUsuario = new UsuarioModel() : this.deleteUsuario = new UsuarioModel();
    this.messageService.clear('c');
    this.varEdit = false;
  }

  onConfirm() {
    this.messageService.clear();
    this.cargando = true;
    this.usuariosPageService.borrarUser(this.deleteUsuario)
      .subscribe(resp => {
        this.usuariosPageService.getUsers().subscribe(respU => {
          this.usuarios = respU as UsuarioModel[];
          this.reiniciaValores();
          this.messageService.clear();
          this.messageService.add({ key: 'tc', severity: 'success', summary: 'Borrado', detail: 'El usuario fue borrado' });
        }, (err) => {
          this.cargando = false;
          console.log(err);
          this.messageService.clear();
          this.messageService.add({ key: 'tc', severity: 'error', summary: 'Error', detail: 'Error al consultar usuarios' });
        });
      }, (err) => {
        this.cargando = false;
        console.log(err);
        this.messageService.clear();
        this.messageService.add({ key: 'tc', severity: 'error', summary: 'Error', detail: 'Error al borrar usuario, por favor inténtelo más tarde.' });
      });

  }
  reiniciaValores() {
    this.newUsuario = new UsuarioModel();
    this.deleteUsuario = new UsuarioModel();
    this.loading = false;
    this.cargando = false;
  }

}
