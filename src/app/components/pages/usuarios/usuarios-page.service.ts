import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {AuthService} from '../../../services/auth.service';
import {UsuarioModel} from '../../../models/usuario/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class UsuariosPageService {


  constructor(private http: HttpClient, private auth: AuthService) { }


  getUsers(){
    return this.http.get(
      `${this.auth.url}users`, {headers: {'Authorization': `Bearer ${this.auth.userToken}`}})
      .pipe(
        map(
          resp => {

            return resp;
          }
        )
      );
  }

  borrarUser( usuario: UsuarioModel ){
    const authData = {
      ...usuario
    };
    return this.http.post(
      `${this.auth.url}users/delete`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
        )
      .pipe(
        map(
          resp => {

            return resp;
          }
        )
      );
  }

  editUser( usuario: UsuarioModel ){
    const authData = {
      ...usuario
    };
    return this.http.post(
      `${this.auth.url}users/edit`, authData ,
      {headers: {'Authorization': `Bearer ${this.auth.userToken}`}},
    )
      .pipe(
        map(
          resp => {

            return resp;
          }
        )
      );
  }
}
