import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResetKeyFormRoutingModule } from './reset-key-form-routing.module';
import { ResetKeyFormComponent } from './reset-key-form.component';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';
import { PasswordModule } from 'primeng/password';
import { FileUploadModule } from 'primeng/fileupload';
import { RippleModule } from 'primeng/ripple';
import { CheckboxModule } from 'primeng/checkbox';
import { LoadingModule } from '../../shared/loading/loading.module';
import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';


@NgModule({
  declarations: [ResetKeyFormComponent],
  imports: [
    CommonModule,
    ResetKeyFormRoutingModule,
    InputTextModule,
    CheckboxModule,
    ButtonModule,
    RippleModule,
    PasswordModule,
    FormsModule,
    FileUploadModule,
    LoadingModule,
    ToastModule
  ]
})
export class ResetKeyFormModule { }
