import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResetKeyFormComponent } from './reset-key-form.component';

const routes: Routes = [{ path: '', component: ResetKeyFormComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResetKeyFormRoutingModule { }
