import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetKeyFormComponent } from './reset-key-form.component';

describe('ResetKeyFormComponent', () => {
  let component: ResetKeyFormComponent;
  let fixture: ComponentFixture<ResetKeyFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetKeyFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetKeyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
