import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TableComponent} from './table.component';
import {TableModule} from 'primeng/table';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {InputTextModule} from 'primeng/inputtext';
import {MultiSelectModule} from 'primeng/multiselect';
import {CalendarModule} from 'primeng/calendar';
import {DropdownModule} from 'primeng/dropdown';
import {ProgressBarModule} from 'primeng/progressbar';



@NgModule({
  declarations: [
    TableComponent
  ],
  imports: [
    CommonModule,

    HttpClientModule,
    FormsModule,
    TableModule,
    InputTextModule,
    MultiSelectModule,
    CalendarModule,
    DropdownModule,
    ProgressBarModule
  ],
  exports: [
    TableComponent
  ]
})
export class TableGeneralModule { }
