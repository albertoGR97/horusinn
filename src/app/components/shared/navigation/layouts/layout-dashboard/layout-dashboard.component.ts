import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    HostBinding,
    Input,
    OnDestroy,
    OnInit,
} from '@angular/core';

import { Subscription } from 'rxjs';
import {NavigationService} from '../../services';
import {SideNavItems, SideNavSection} from '../../models';
import { Router } from '@angular/router';

@Component({
    selector: 'sb-layout-dashboard',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './layout-dashboard.component.html',
    styleUrls: ['layout-dashboard.component.scss'],
})
export class LayoutDashboardComponent implements OnInit, OnDestroy {
    @Input() static = false;
    @Input() light = false;
    @HostBinding('class.sb-sidenav-toggled') sideNavHidden = true;
    subscription: Subscription = new Subscription();
    sideNavSections: SideNavSection[];
    sideNavSectionsAdmin: SideNavSection[] = [
      {
        items: ['ambientes'],
      },
      {
        items: ['administrar'],
      },
      {
        items: [ 'empresas'],
      },
      {
        items: ['lectores', 'servidores'],
      },
      {
        items: [ 'administracion'],
      },
    ];
    sideNavSectionsUser: SideNavSection[] = [
      {
        items: ['ambientes'],
      },
    ];
    sideNavItems: SideNavItems = {
      ambientes: {
        icon: 'cogs',
        text: 'Ambientes',
        submenu: [
          {
            text: 'Creación',
            submenu: [
              {
                text: 'Girha',
                link: '/girha/ambientes',
              },
              {
                text: 'Mi Asistencia',
                link: '/mi_asistencia/ambientes',
              },
              {
                text: 'BioTime Cloud',
                link: '/biotimecloud/ambientes',
              },
            ],
          },
          {
            text: 'Destrucción',
            submenu: [
              {
                text: 'Girha',
                link: '/girha/destruccion',
              },
              {
                text: 'Mi Asistencia',
                link: '/mi_asistencia/ambientes',
              },
              {
                text: 'BioTime Cloud',
                link: '/biotimecloud/destruccion',
              },
            ],
          },
        ]
      },
      administrar:{//nueva pestaña que aquí dira administración y aparecera en la ventana de inicio.
        icon: 'table', //la iconografia pertenece a fontaweson revisar documentacion ChartPie
        text: 'Administración', //texto que debe ir en la pestaña 
        link: '/administracion/administracion',//link que redireccionara al aplicativo
      },
      empresas: {
        icon: 'building',
        text: 'Empresas',
        submenu: [
          {
            text: 'Girha',
            submenu: [
              {
                text: 'Alertas Respaldos',
                link: '/empresas/girha/girha-alertas'
              }
            ],
          },
        ],
      },
      lectores: {
        icon: 'pager',
        text: 'Lectores',
        link: '/lectores'
      },
      servidores: {
        icon: 'server',
        text: 'Servidores',
        link: '/servidores',
      },
      administracion: {
        icon: 'cog',
        text: 'Configuración',
        submenu: [
          {
            text: 'Usuarios',
            link: '/usuarios',
          },
          {
            text: 'Lectores',
            link: '/configuracion/lectores',
          },

        ],
      },

    };

    sidenavStyle = 'sb-sidenav-dark';

    constructor(public navigationService: NavigationService,private changeDetectorRef: ChangeDetectorRef, private router: Router) {
      if(localStorage.getItem('perfil') === "1"){
        this.sideNavSections = this.sideNavSectionsAdmin;
      } else {
        this.sideNavSections = this.sideNavSectionsUser;
      }

    }

    ngOnInit() {
        if (this.light) {
            this.sidenavStyle = 'sb-sidenav-light';
        }
        this.subscription.add(
            this.navigationService.sideNavVisible$().subscribe(isVisible => {
                this.sideNavHidden = this.router.url == '/home' ? isVisible : !isVisible;
                this.changeDetectorRef.markForCheck();

            })
        );
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
