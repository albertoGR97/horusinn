import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {NavigationService} from '../../services';


@Component({
    selector: 'sb-top-nav',
    templateUrl: './top-nav.component.html',
    styleUrls: ['top-nav.component.scss'],
})
export class TopNavComponent implements OnInit {
    constructor(private navigationService: NavigationService) {}
    ngOnInit() {}


    toggleSideNav() {
        this.navigationService.toggleSideNav();
    }

    fechaActual = new Date();
    FechaMostrar = this.fechaActual.getFullYear()+'-'+(this.fechaActual.getMonth()+1)+'-'+this.fechaActual.getDate();
}
